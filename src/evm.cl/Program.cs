﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Evm.Schema;
using System.Runtime.InteropServices;
using log4net;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;

namespace Evm.cl
{
    /// <summary>
    /// Evm 命令行编译局工具
    /// 参数：
    /// /out:输出目录。默认当前目录
    /// /cc:[][,]所选编译器。默认scs
    /// /evmlibdir:evmlib目录。默认当前目录
    /// /ci:其他编译器指令
    /// app.xml 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enterprise Virtual Machine(C) Compiler Tools Ver:" + typeof(Program).Assembly.GetName().Version.ToString());
            Console.WriteLine( ".Net Framework Version:" + RuntimeEnvironment.GetSystemVersion());
            Console.WriteLine();
            
            ILog logger = LogManager.GetLogger(typeof(Program));
            if (args.Length == 0)
            {
                Console.WriteLine("用法：evmc /appdir:appdir /cc:[scs [c:v]  [c2:v2]][,cjs [c:v]  [c2:v2]] /output:outputdir  /bindir:bindir [app.xml]");
                Console.WriteLine("/appdir:appdir App存储根目录。默认当前目录");
                Console.WriteLine("/cc:[scs [c:v]][,cjs [c:v]] 所选编译器。默认使用scs编译器，可以同时指定多个。 [c:v]是编译器特有参数。");
                Console.WriteLine("/bindir:evm Bin目录。evmc所在目录");
                Console.WriteLine("/output:输出目录");
                Console.WriteLine("/zip:是否打包应用");
                Console.WriteLine("app.xml 需要编译的应用程序定义");
                return;
            }

            string appDir, binDir, appDefXmlName,outputDir;
            bool zipApp = false;
            var ccList = new List<Compiler>();
            if (!ParseArgs(args,out appDir,out binDir,out  appDefXmlName,out outputDir, ccList,out zipApp))
                return;
            
            Evm.CompilerBase2 c;
            App app = AppBuilder.Instance.Parse(appDefXmlName);
            var cc = new List<Evm.CompilerBase2>();
            if (ccList.Count == 0)
            {
                ccList.Add(new Compiler() {  Name="scs"});
            }
           
            for (int i = 0; i < ccList.Count; i++)
            {
                if (ccList.ElementAt(i).Name== "scs")
                {
                    c = new Evm.Compiler.Server.Core.CoreCompiler();
                    c.Configuration = ccList.ElementAt(i).Configurations;
                    cc.Add(c);
                }
                if (ccList.ElementAt(i).Name == "aspx")
                {
                   
                    //c.Compile(app, outDir, evmLibDir, outDir + "\\Web\\", ccList["aspxmvc"]);
                    c = new Evm.Compiler.Aspx.AspxCompiler();
                    c.Configuration = ccList.ElementAt(i).Configurations;
                    cc.Add(c);
                }
                
            }
            Evm.Configuration.Environment.Instance.AppFolder = appDir;
            Evm.Configuration.Environment.Instance.BinDir = binDir;
            if (outputDir == ".")
                outputDir = Environment.CurrentDirectory;
            Evm.Configuration.Environment.Instance.OutputDir = outputDir;
            AppBuilder.Instance.Compile(appDefXmlName,cc);
            Console.WriteLine("编译完毕！");
            Console.Write(string.Format(@"正在压缩应用源文件{0}\{1}.zip ...",outputDir,app.Name));
            if (zipApp)
            {
                var zip = ZipFile.Create(string.Format(@"{0}\{1}.zip",outputDir,app.Name));
                zip.Add(appDefXmlName);
                foreach (var item in app.EntityDefinitions)
                {
                    zip.Add(item);
                }
                foreach (var item in app.ServiceDefinitions)
                {
                    zip.Add(item);
                }
                
                zip.Close();
            }
            Console.WriteLine("ok.");
        }
        /// <summary>
        /// 解析命令行参数sh
        /// </summary>
        /// <param name="args">命令行参数集</param>
        /// <param name="appDir">应用存放目录</param>
        /// <param name="binDir">evm bin存放目录</param>
        /// <param name="appDefXmlName">应用定义文件</param>
        /// <param name="outputDir">输出目录</param>
        /// <param name="ccList">编译器及参数列表</param>
        /// <param name="zipApp">是否打包zip文件</param>
        /// <returns></returns>
        static bool ParseArgs(string[] args, out string appDir, out string binDir, out string appDefXmlName,out string outputDir, List<Compiler> ccList,out bool zipApp)
        {
            appDir = Environment.CurrentDirectory;
            binDir=appDir;
            appDefXmlName = "app.xml";
            outputDir = ".";
            zipApp = false;
            for (var i = 0; i < args.Length;i++ )
            {
                var item = args[i];
                var arg = item.Trim().ToLower();
                if (arg.IndexOf(":") < 0 || arg.IndexOf(":") == 1)
                {
                    appDefXmlName = item;
                    continue;
                }
                var argName = arg.Substring(0, arg.IndexOf(":"));
                var argValue = arg.Substring(arg.IndexOf(":") + 1);

                if (argName == "/appdir")
                    appDir = argValue;
                else if (argName == "/bindir")
                    binDir = argValue;
                else if (argName == "/output")
                    outputDir = argValue;
                else if (argName == "/zip")
                {
                    if (!string.IsNullOrEmpty(argValue))
                        zipApp = Convert.ToBoolean(argValue);
                    else
                        zipApp = true;
                }
                else if (argName == "/cc")
                {
                    //确定后续有没有编译器特定参数
                    i++;//转到下一个参数
                    while (i < args.Length)
                    {
                        //第一个字符不为"/"（下一个参数）和","（下一个编译器）且，包含":"则可确定为编译器参数
                        if (args[i].Trim().Substring(0, 1) != "/" && args[i].IndexOf(":") > 1) //自定义参数不允许一个字符的参数名
                        {
                            argValue += " " + args[i].Trim();
                            i++;
                        }
                        else
                        {
                            i--;//退回一个参数位
                            break;
                        }
                    }
                    string[] ccs = argValue.Split(new char[] { ',' });
                    //拆分编译器和特定参数
                    foreach (var cs in ccs)
                    {
                        var csvs = cs.Split(new char[] { ' ' });
                        var c = new Compiler();
                        c.Name = csvs[0];
                        for (var j = 1; j < csvs.Length; j++)
                        {
                            var loc = csvs[j].IndexOf(":");
                            c.Configurations.Add(csvs[j].Substring(0, loc), csvs[j].Substring(loc + 1));
                        }
                        ccList.Add(c);
                    }
                }
                else
                {
                    Console.WriteLine("非法参数：" + arg);
                    return false;
                }
            }
            return true;
        }
        public class Compiler
        {
            public Compiler()
            {
                Configurations = new System.Collections.Generic.Dictionary<string, string>();
            }
            public string Name { get; set; }
            public IDictionary<string,string> Configurations {get;set;}
        }
    }
}
