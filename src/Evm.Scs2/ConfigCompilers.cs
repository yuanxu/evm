﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Compiler.Server.Core
{
    internal class SqlMapConfigCompiler
    {
        public static void Compiler(string tempDir, Evm.Schema.App app)
        {
            var sqlMapConfig = Utils.TplUtil.ParseFromResource<SqlMapConfigStateObject>("Evm.Compiler.Server.Core.Templates.SqlMap.config.cshtml", new SqlMapConfigStateObject() { DbProvider = app.Options.DbProvider, Entities = app.Entities, ConnectionString =app.Options.DataSource });
            Utils.TplUtil.WriteToFile(string.Format("{0}\\{1}\\SqlMap.config", tempDir, app.Name), sqlMapConfig);

            var providerConfig = Utils.TplUtil.ParseFromResource<string>("Evm.Compiler.Server.Core.Templates.providers.config.cshtml",app.Options.DbProvider);
            Utils.TplUtil.WriteToFile(string.Format("{0}\\{1}\\providers.config", tempDir, app.Name), providerConfig);

        }

    }
    public class SqlMapConfigStateObject
    {
        public string DbProvider { get; set; }
        public IList<Schema.Entity> Entities { get; set; }
        public string ConnectionString { get; set; }
    }


}
