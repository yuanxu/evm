﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace Evm.Compiler.Server.Core
{
    /// <summary>
    /// 核心编译器
    /// <remarks>生成实体、服务、接口等的主编译器</remarks>
    /// </summary>
    public class CoreCompiler:CompilerBase2 
    {
        EvmObjectCompilerBase mapCompiler, modelCompiler,idaoCompiler,iserviceCompiler,daoCompiler,svcCompiler,enumCompiler;

        public CoreCompiler():base()
        {
            mapCompiler = new SqlMapCompiler();
            modelCompiler = new ModelCompiler();
            idaoCompiler = new IDaoCompiler();
            iserviceCompiler = new IServiceCompiler();
            daoCompiler = new DaoImpCompiler();
            svcCompiler = new ServiceImplCompiler();
            enumCompiler = new EnumCompiler();
            
            AddEvmObjectCompiler(mapCompiler);
            AddEvmObjectCompiler(modelCompiler);
            AddEvmObjectCompiler(idaoCompiler);
            AddEvmObjectCompiler(iserviceCompiler);
            AddEvmObjectCompiler(daoCompiler);
            AddEvmObjectCompiler(svcCompiler);
            AddEvmObjectCompiler(enumCompiler);
        }

        
      
        public override string FriendlyName
        {
            get { return "核心编译器第二版"; }
        }

        public override string Name
        {
            get { return "Core Compiler verion2"; }
        }

        /// <summary>
        /// 覆盖标准编译方法
        /// <remarks>本编译器要生成.model;.def,.impl三个dll，与标准编译方法不同。故重写之。</remarks>
        /// </summary>
        public override void Compile()
        {
            Log.Debug(OutputDir);
            if (!System.IO.Directory.Exists(OutputDir))
                System.IO.Directory.CreateDirectory(OutputDir);
            ClearUp(TempDir+"\\"+App.Name);

           // OutputDir = string.Format("{0}\\{1}", Directory.GetCurrentDirectory() , OutputDir);
            Utils.TraceTime tt = new Utils.TraceTime(Log);
            tt.ReInitWhenStatistic = true;
            //生成模型
            foreach (var entity in App.Entities)
            {
                Log.InfoFormat("正在解析{0}...",entity.FileName);
                //生成模型定义
                if (entity.IsEnum)
                {
                    enumCompiler.Compile(entity);
                }
                else
                {
                    modelCompiler.Compile(entity);
                    //生成Dao接口定义
                    idaoCompiler.Compile(entity);
                    //生成SqlMap
                    mapCompiler.Compile(entity);
                    //生成Dao实现
                    daoCompiler.Compile(entity);
                }
            }
            tt.Statistic("解析模型");
           //生成服务
            foreach (var svc in App.Services)
            {
                Log.InfoFormat("正在解析{0}...",svc.FileName);
                //生成服务接口定义
                if(svc.ServiceContract== Schema.ServiceContract.Public)
                    iserviceCompiler.Compile(svc);
                //生成服务实现
                svcCompiler.Compile(svc);
            }

            //生成服务
            var appCode = Utils.TplUtil.ParseFromResource<Schema.App>("Evm.Compiler.Server.Core.Templates.App.cshtml", App);
            Utils.TplUtil.WriteToFile(string.Format("{0}\\{1}\\Impl\\{1}App.cs", TempDir, App.Name), appCode);

            tt.Statistic("解析服务");
            //生成SqlMap.Config和
            SqlMapConfigCompiler.Compiler(TempDir, App);

            var refAssemblies = "";
            foreach (var asm in App.Assemblies)
            {
                refAssemblies += string.Format(" /r:{0} ",asm);
            }
           //编译模型
            Log.InfoFormat("开始编译模型定义{0}.Model.dll...",App.Name);
            ///optimize
            var cmdLine = string.Format(" /target:library /out:{0}\\{1}.Model.dll /debug  /r:Evm.Rtl.dll /r:System.ComponentModel.DataAnnotations.dll /r:Newtonsoft.Json.dll /lib:{2} {3}  *.cs "
                , OutputDir, App.Name, Evm.Configuration.Environment.Instance.BinDir, refAssemblies);
            RunCsc(cmdLine, string.Format(@"{0}\{1}\Model\", TempDir,App.Name));
            Log.InfoFormat("{0}.Model.dll生成成功.",App.Name);

            tt.Statistic(string.Format("编译{0}.Model.dll",App.Name));

            Log.InfoFormat("开始编译接口/契约{0}.IDef.dll...",App.Name);
            //编译接口
            cmdLine = string.Format(" /target:library /out:{0}\\{1}.IDef.dll /optimize  /r:Evm.Rtl.dll /r:{1}.Model.dll /lib:{0}  /lib:{2} {3}  *.cs "
                , OutputDir, App.Name, Evm.Configuration.Environment.Instance.BinDir, refAssemblies);
            RunCsc(cmdLine, string.Format(@"{0}\{1}\IDef\", TempDir, App.Name));
            Log.InfoFormat("{0}.IDef.dll生成成功.",App.Name);

            tt.Statistic(string.Format("编译{0}.IDef.dll", App.Name));

           //编译实现
            Log.InfoFormat("开始编译实现库b{0}.ServiceImpl.dll",App.Name);
            var eres = new StringBuilder();
            foreach (var item in App.Entities)
            {
                if (item.IsEnum) 
                    continue;
                eres.Append( string.Format(@" /resource:..\Map\{0}.xml", item.Name));
            }
            //types
            var code = Utils.TplUtil.ParseFromResource<Schema.App>("Evm.Compiler.Server.Core.Templates.Types.cshtml", App);
            Utils.TplUtil.WriteToFile(string.Format(@"{0}\{1}\Map\Types.xml", TempDir, App.Name), code);
            
            eres.Append(string.Format(@" /resource:..\Map\Types.xml"));

            eres.Append(string.Format(@" /resource:..\SqlMap.config"));
            eres.Append(string.Format(@" /resource:..\providers.config"));
            // /optimize
            cmdLine = string.Format(" /target:library /out:{0}\\{1}.ServiceImpl.dll /debug /r:Evm.Rtl.dll /r:{1}.Model.dll /r:{1}.IDef.dll /r:IBatisNet.DataMapper.dll /r:IBatisNet.Common.dll /r:System.ComponentModel.DataAnnotations.dll /lib:{0} /lib:{2} {3} {4} *.cs "
                , OutputDir, App.Name, Evm.Configuration.Environment.Instance.BinDir, eres, refAssemblies);
            RunCsc(cmdLine, string.Format(@"{0}\{1}\Impl\", TempDir, App.Name));
            Log.InfoFormat("{0}.ServiceImpl.dll生成成功.",App.Name);

            tt.Statistic(string.Format("编译{0}.ServiceImpl.dll", App.Name));
        }

        private void ClearUp(string TempDir)
        {
            var dirs = new[] { "IDef","Impl","Map","Model"};
            foreach (var dir in dirs)
            {
                var tdir = string.Format("{0}\\{1}", TempDir, dir);
                if (Directory.Exists(tdir))
                {
                    foreach (var file in Directory.GetFiles(tdir))
                    {
                        File.Delete(file);
                    }
                }
            }
        }
    }

    
}
