﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Compiler.Server.Core
{
    public class EnumCompiler : ModelCompiler 
    {
        protected override string DoCompile(object t)
        {
            var obj = t as Schema.Entity;
            if (!obj.IsEnum)
                return "";
            else
                return Utils.TplUtil.ParseFromResource<Schema.Entity>("Evm.Compiler.Server.Core.Templates.Enum.cshtml", obj);
            
        }

        public override bool IsEntityCompiler
        {
            get { return true; }
        }

        public override bool IsServiceCompiler
        {
            get { return false; }
        }
    }
}
