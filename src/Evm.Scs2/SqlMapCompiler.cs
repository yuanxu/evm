﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Evm.Compiler.Server.Core
{
    /// <summary>
    /// 映射文件编译器
    /// </summary>
    public class SqlMapCompiler : EvmObjectCompilerBase
    {
       
        public SqlMapCompiler():base()
        {
          
        }
        
        protected override string DoCompile(object t)
        {
            return Utils.TplUtil.ParseFromResource<Schema.Entity>("Evm.Compiler.Server.Core.Templates.SqlMap.cshtml", t as Schema.Entity);
        }

        public override bool IsEntityCompiler
        {
            get { return true; }
        }

        public override bool IsServiceCompiler
        {
            get { return false; }
        }
        public override void Setup(Schema.App app, string outputDir, string tempDir)
        {
            base.Setup(app, outputDir, tempDir);
            if (!Directory.Exists(string.Format("{0}\\{1}\\Map",TempDir,app.Name)))
                Directory.CreateDirectory(string.Format("{0}\\{1}\\Map",TempDir,app.Name));
        }
        protected override void EndCompile(object t, string code)
        {
            base.EndCompile(t, code);
            var fileName = string.Format("{0}\\{1}\\Map\\{2}.xml",TempDir,App.Name,(t as Schema.Entity).Name);
            Utils.TplUtil.WriteToFile(fileName, code);
            TargetFiles.Add(fileName);
        }
    }
}
