﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Evm.Compiler.Server.Core
{
    public class IDaoCompiler:EvmObjectCompilerBase
    {
        protected override string DoCompile(object t)
        {
            return Utils.TplUtil.ParseFromResource<Schema.Entity>("Evm.Compiler.Server.Core.Templates.IDao.cshtml", t as Schema.Entity);
        }
        protected override void EndCompile(object t, string code)
        {
            base.EndCompile(t, code);
            var fileName = string.Format("{0}\\{1}\\IDef\\I{2}Dao.cs", TempDir, App.Name, (t as Schema.Entity).Name);
            Utils.TplUtil.WriteToFile(fileName, code);
            TargetFiles.Add(fileName);
        }
        public override void Setup(Schema.App app, string outputDir, string tempDir)
        {
            base.Setup(app, outputDir, tempDir);
            if (!Directory.Exists(string.Format("{0}\\{1}\\IDef", TempDir, app.Name)))
                Directory.CreateDirectory(string.Format("{0}\\{1}\\IDef", TempDir, app.Name));
        }

        public override bool IsEntityCompiler
        {
            get { return true; }
        }

        public override bool IsServiceCompiler
        {
            get { return false; }
        }
    }
}
