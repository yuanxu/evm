﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Compiler.Server.Core
{

    /// <summary>
    /// 编译起辅助方法
    /// </summary>
    public static class CompilerHelper
    {
        /// <summary>
        /// 获取方法签名
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public static string GetSign(Schema.Method method)
        {
            
            string result = "";
            if (method.IsReturnList)
                if (method.SupportPaging)
                    result = "PagedList<" + method.ResultClass + ">";
                else
                    result = "IList<" + method.ResultClass + ">";
            else if (string.IsNullOrEmpty(method.ResultClass))
                result = "void";
            else
                result = method.ResultClass;
            
            return string.Format("{0} {1}( {2} )", result,method.ID, method.Parameters);
        }

        /// <summary>
        /// 获取语句签名
        /// </summary>
        /// <param name="stm"></param>
        /// <returns></returns>
        public static string GetSign(Schema.Statement stm)
        {
            string result;
           
            
            if (stm.IsReturnList)
                if (stm.SupportPaging)
                    result = "PagedList<" + stm.ResultClass + ">";
                else
                    result = "IList<" + stm.ResultClass + ">";
            else if (string.IsNullOrEmpty(stm.ResultClass))
                result = "void";
            else
                result = stm.ResultClass;

            return string.Format("{0} {1}( {2} )", result,stm.ID, stm.Parameters);
        }

        public static string GetDaoMethodImpl(Schema.Statement item)
        {
            var isUpdate = item.TagName == "update";

            StringBuilder sbHt = new StringBuilder();

            //准备参数设置
            string qp = ""; //传递给MyMapper的参数
            if (item.HasParameter)
            {
                var parameters = Utils.MiscUtil.SplitParameters( item.Parameters);//.Split(new char[] { ',' });
                //多个参数
                if (item.SupportPaging && parameters.Count == 3) //实际是一个参数
                {
                    qp = parameters[0].Substring(parameters[0].IndexOf(" ") + 1).Trim();
                }
                else if (item.Parameters.IndexOf(",") >= 0  || isUpdate)
                {
                    qp = "ht";
                    sbHt.AppendLine("Hashtable ht = new Hashtable();");
                    foreach (var p in parameters)
                    {
                        var p2 = Evm.Utils.MiscUtil.GetVarName(p);

                        if (item.SupportSort && (p2 == "sortColumn" || p2 == "sortDirection"))
                            continue;
                        sbHt.AppendLine(string.Format("ht.Add(\"{0}\",{0});", p2));
                    }
                    if (item.SupportSort)
                        sbHt.AppendLine("GetSortHashtable(ht,sortColumn,sortDirection);");
                    if (isUpdate)
                        sbHt.AppendLine("ht.Add(\"EvmLastModifyTime\",DateTime.Now);");
                }
                else
                {
                    qp = item.Parameters.Substring(item.Parameters.IndexOf(" ") + 1).Trim();
                }
            }
            else
                qp = "null";
            if (item.SupportPaging && item.IsReturnList)//分页的列表
            {
                sbHt.AppendLine(string.Format("return new PagedList<{0}>(pageIndex , MyMapper.QueryForList<{0}>(\"{1}.{2}\",{3},{4},{5}) , MyMapper.QueryForObject<int>(\"{1}.{2}Count\",{3}));"
                    ,item.ResultClass
                    , item.Entity.Name
                    , item.ID
                    , qp
                    , "(pageIndex-1)*pageSize"
                    , "pageSize"));
            }
            else if (item.IsReturnList)//为列表
            {
                sbHt.AppendLine( string.Format("return MyMapper.QueryForList<{0}>(\"{1}.{2}\",{3});"
                    ,item.ResultClass
                    , item.Entity.Name
                    , item.ID
                    , qp
                    ));
            }
            else //其他类型语句
            {
                if (item.TagName == "select")
                {
                    if (item.ResultClass == "object" || item.ResultClass=="")
                    {
                        sbHt.AppendLine(string.Format("return MyMapper.QueryForObject(\"{1}.{2}\",{3});"
                            , item.ResultClass
                            , item.Entity.Name
                            , item.ID
                            , qp
                        ));
                    }
                    else
                    {
                        sbHt.AppendLine(string.Format("return MyMapper.QueryForObject<{0}>(\"{1}.{2}\",{3});"
                            , item.ResultClass
                            , item.Entity.Name
                            , item.ID
                            , qp
                        ));
                    }
                }
                else
                {
                    var ret = "";
                    if (!string.IsNullOrEmpty(item.ResultClass))
                       ret="return ";
                    sbHt.Append(ret);
                    if (item.TagName == "insert")
                    {
                        sbHt.Append("Convert.ToInt32(");
                    }
                    string method = item.TagName.Substring(0, 1).ToUpper() + item.TagName.Substring(1);
                    sbHt.Append( string.Format("MyMapper.{0}(\"{1}.{2}\",{3})"
                        , method
                        , item.Entity.Name
                        , item.ID
                        , qp));
                    if (item.TagName == "insert")
                    {
                        sbHt.Append(")");
                    }
                    sbHt.Append(";\r\n");
                }
            }
            return sbHt.ToString();
        }

        public static string GetSvcImpl(Schema.Method method)
        {
            string result;
            ///StringBuilder sbParam = new StringBuilder();
            
            if (method.IsReturnList)
                if (method.SupportPaging)
                    result = "PagedList<" + method.ResultClass + ">";
                else
                    result = "IList<" + method.ResultClass + ">";
            else if (string.IsNullOrEmpty(method.ResultClass))
                result = "void";
            else
                result = method.ResultClass;
            if (result != "void" && method.TransactionModel == Schema.TransactionModel.RequiredNew)
                throw new Schema.SchemaException(string.Format("声明需要新事物(RequiredNew)的方法不能具有返回值[{2}]！({0}.{1})", method.Service.Name, method.ID,result));

           // AppendToBuilder(sbParam, method.Parameters);


            //准备参数设置
            StringBuilder sbStm = new StringBuilder();
            if (!string.IsNullOrEmpty(method.RefStatement)) //直接引用Dao
            {
                //准备事务语句
                PrepareTrans(method, sbStm);
                if (result != "void")
                    sbStm.Append("return ");
                var referedEntity = method.BeReferencedStatements[0].Entity.Name;
                var call = Evm.Utils.RefUtils.Replace(method.RefStatement, referedEntity, referedEntity + "Dao");
                sbStm.Append(call);
                sbStm.Append("(");
                if (method.HasParameter) //有参数
                {
                    bool added = false;

                    foreach (var kv in method.ParametersDefinition)
                    {
                        //Console.WriteLine(kv.Key+"::"+kv.Value);
                        //string v = Evm.Utils.MiscUtil.GetVarName(item);

                        if (added)
                            sbStm.Append(",");
                        sbStm.Append(kv.Value);
                        added = true;
                    }
                }
                sbStm.Append(");");
                //完成事务语句
                FinishTrans(method, sbStm);
            }
            else //签入原生C#代码
            {

                //准备事务语句
                PrepareTrans(method, sbStm);

                var code = method.EmbedCode;
                foreach (var item in method.ReferencedMethods)
                {
                    string tsvc;
                    if (item.Service.Name == method.Service.Name)
                        tsvc = "this";
                    else
                        tsvc = item.Service.Name;
                    code = Evm.Utils.RefUtils.Replace(code, item.Service.Name, tsvc);
                }
                foreach (var item in method.BeReferencedStatements)
                {
                    code = Evm.Utils.RefUtils.Replace(code, item.Entity.Name, item.Entity.Name + "Dao");
                }
                sbStm.Append(code);

                //完成事务语句
                FinishTrans(method, sbStm);
            }
            return sbStm.ToString();
        }

        /// <summary>
        /// 准备事务
        /// </summary>
        /// <param name="method"></param>
        /// <param name="sbStm"></param>
        private static void PrepareTrans(Schema.Method method, StringBuilder sbStm)
        {
            if (method.TransactionModel == Schema.TransactionModel.Required) //添加检验事务是否存在语句
                sbStm.AppendLine("AssertInTransaction();");
            else if (method.TransactionModel == Schema.TransactionModel.NotSupport) //添加检验事务不存在语句
                sbStm.AppendLine("AssertNotInTransaction();");
            else if (method.TransactionModel == Schema.TransactionModel.RequiredNew)//添加事务管理
            {
                sbStm.AppendLine(" AssertTransactionNotStart();");
                sbStm.AppendLine("BeginTransaction();");
                sbStm.AppendLine("try");
                sbStm.AppendLine("{");
            }
        }

        private static void FinishTrans(Schema.Method method, StringBuilder sbStm)
        {
            if (method.TransactionModel == Schema.TransactionModel.RequiredNew)
            {
                sbStm.AppendLine("Commit();");
                sbStm.AppendLine("}");
                sbStm.AppendLine("catch");
                sbStm.AppendLine("{");
                sbStm.AppendLine("Rollback();");
                sbStm.AppendLine("throw;");
                sbStm.AppendLine("}");
            }

        }
    }
}
