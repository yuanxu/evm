﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Evm.Compiler.Server.Core
{
    public class IServiceCompiler:EvmObjectCompilerBase
    {
        protected override string DoCompile(object t)
        {
            return Utils.TplUtil.ParseFromResource<Schema.Service>("Evm.Compiler.Server.Core.Templates.IService.cshtml", t as Schema.Service);
        }
        protected override void EndCompile(object t, string code)
        {
            base.EndCompile(t, code);
            var fileName = string.Format("{0}\\{1}\\IDef\\I{2}.cs", TempDir, App.Name, (t as Schema.Service).Name);
            Utils.TplUtil.WriteToFile(fileName, code);
            TargetFiles.Add(fileName);
        }
        public override void Setup(Schema.App app, string outputDir, string tempDir)
        {
            base.Setup(app, outputDir, tempDir);
            if (!Directory.Exists(string.Format("{0}\\{1}\\IDef", TempDir, app.Name)))
                Directory.CreateDirectory(string.Format("{0}\\{1}\\IDef", TempDir, app.Name));
        }

        public override bool IsEntityCompiler
        {
            get { return false; }
        }

        public override bool IsServiceCompiler
        {
            get { return true; }
        }
    }
}
