﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm
{
    public class EvmException:ApplicationException
    {
         public EvmException(string message) : base(message) { }
         public EvmException(string message, Exception inner) : base(message, inner) { }
    }
}
