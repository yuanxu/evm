﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using Evm.Schema;
using System.Diagnostics;
using System.IO;

namespace Evm
{
    /// <summary>
    /// 增强版编辑器基类
    /// </summary>
    public abstract class CompilerBase2
    {
        public CompilerBase2()
        {
            EvmObjectCompilers = new List<EvmObjectCompilerBase>();
        }
        ILog log;

        public ILog Log
        {
            get
            {
                if (log == null)
                    log = LogManager.GetLogger(this.GetType());
                return log;
            }
        }
        /// <summary>
        /// 编译器友好名称
        /// </summary>
        public abstract string FriendlyName { get; }

        /// <summary>
        /// 编译器名称
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// 提供编译器的附加信息
        /// </summary>
        public virtual string Information { get { return ""; } }

        /// <summary>
        /// 待编译应用
        /// </summary>
        public App App { get; protected set; }

        /// <summary>
        /// 输出目录
        /// </summary>
        public string OutputDir { get;  protected set; }

        /// <summary>
        /// 临时目录
        /// </summary>
        public string TempDir { get; protected set; }

        /// <summary>
        /// 编译器专有设置
        /// </summary>
        public IDictionary<string, string> Configuration { get;set; }

        /// <summary>
        /// 设置编译器参数
        /// </summary>
        /// <param name="app"></param>
        /// <param name="outputDir"></param>
        /// <param name="tempDir"></param>
        public virtual void Setup(App app, string outputDir, string tempDir, Dictionary<string, string> configuration = null)
        {
            App = app;
            OutputDir = outputDir;
            TempDir = tempDir;
            Configuration = configuration;
            foreach (var c in EvmObjectCompilers)
            {
                c.Setup(app, outputDir, tempDir);
            }
        }
        /// <summary>
        /// 执行编译
        /// </summary>
        public virtual void Compile()
        {
            OnBeginCompile(); //启动编译事件
            if (!System.IO.Directory.Exists(OutputDir))
                System.IO.Directory.CreateDirectory(OutputDir);
            // OutputDir = string.Format("{0}\\{1}", Directory.GetCurrentDirectory(), OutputDir);
            //编译实体
            foreach (var compiler in EvmObjectCompilers)
            {
                if (compiler.IsEntityCompiler)
                {
                    foreach (var item in App.Entities)
                    {
                        compiler.Compile(item);
                    }
                    compiler.OnCompileAll();//告知对象编译器已完成所有编译工作
                }
                else if (compiler.IsServiceCompiler)
                {
                    foreach (var item in App.Services)
                    {
                        compiler.Compile(item);
                    }
                    compiler.OnCompileAll();//告知对象编译器已完成所有编译工作
                }
            }
            OnEndCompile();//所有编译执行完毕
        }

        /// <summary>
        /// 本编译器所有的对象编译器集合
        /// </summary>
        public IList<EvmObjectCompilerBase> EvmObjectCompilers { get; set; }

        /// <summary>
        /// 添加附属编译器
        /// </summary>
        /// <param name="c"></param>
        public void AddEvmObjectCompiler(EvmObjectCompilerBase c)
        {
            if (EvmObjectCompilers == null)
                throw new NullReferenceException();
            c.MainCopmiler = this;
            EvmObjectCompilers.Add(c);
        }
        /// <summary>
        /// 本编译器执行完毕。即已经完成所有对象编译工作。此事件为编译器完成编译后的清理工作提供一个机会
        /// </summary>
        public event EventHandler EndCompile;

        /// <summary>
        /// 开始执行编译。此事件为编译器进行编译前的准备工作提供一个机会。
        /// </summary>
        public event EventHandler BeginCompile;

        internal void OnEndCompile()
        {
            if (EndCompile != null)
                EndCompile(this, new EventArgs());
        }

        internal void OnBeginCompile()
        {
            if (BeginCompile != null)
                BeginCompile(this, new EventArgs());
        }

        /// <summary>
        /// 调用编译器编译。
        /// </summary>
        /// <param name="cmdLine"></param>
        /// <param name="workDir"></param>
        /// <returns></returns>
        protected void RunCsc(string cmdLine, string workDir)
        {
            // System.Environment.CurrentDirectory = workDir;
            var cscDir = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory();
            using (Process process = new System.Diagnostics.Process())
            {
                process.StartInfo.FileName = string.Format(@"{0}\csc", cscDir);
                process.StartInfo.Arguments = cmdLine;

                // Logger.Info(string.Format("{0} {1}",process.StartInfo.FileName,cmdLine));

#if DEBUG
                Debug.Print(string.Format("{0} {1}", process.StartInfo.FileName, cmdLine));
                log.Info(string.Format("{0} {1}", process.StartInfo.FileName, cmdLine));
#endif

                // 必须禁用操作系统外壳程序  
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.WorkingDirectory = workDir;
                // 为异步获取订阅事件  
                process.OutputDataReceived += new DataReceivedEventHandler(process_OutputDataReceived);

                process.Start();
                //var s = process.StandardOutput.ReadToEnd();

                // 异步获取命令行内容  
                process.BeginOutputReadLine();
                process.WaitForExit();
                var exitCode =  process.ExitCode;
                if (exitCode != 0)
                {
                    throw new EvmCompileException(string.Format("CS编译器返回值异常:{0}",exitCode));
                }
            }
        }

        private void process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            // 这里仅做输出的示例，实际上您可以根据情况取消获取命令行的内容  
            // 参考：process.CancelOutputRead()  
            if (!string.IsNullOrEmpty(e.Data) && e.Data.IndexOf("Microsoft") < 0 && e.Data.IndexOf("版权所有") < 0)
            {
                Log.Info(e.Data);
#if DEBUG
                Debug.Print(e.Data);
#endif
            }
        }

    }
}
