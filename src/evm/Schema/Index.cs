﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.Schema
{
    /// <summary>
    /// 索引
    /// </summary>
    [Serializable]
    public class Index
    {
        /// <summary>
        /// 索引名称
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        /// 索引字段
        /// </summary>
        [XmlAttribute("columns")]
        public string Columns { get; set; }

        /// <summary>
        /// 所属实体
        /// </summary>
        [XmlIgnore]
        public Entity Entity { get;  set; }

        /// <summary>
        /// 是否是唯一索引
        /// </summary>
        [XmlAttribute("isUnique")]
        public bool IsUnique { get; set; }
    }
}
