﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using log4net;
namespace Evm.Schema
{
    /// <summary>
    /// Evm应用程序
    /// </summary>
    [Serializable]
    [XmlRoot("app",Namespace="http://evm.org/app")]
    public partial class App
    {

        ILog log = LogManager.GetLogger(typeof(App));
        public App()
        {
            Entities = new List<Entity>();
            Services = new List<Service>();
            Imports=new string[0];
            Assemblies = new string[0];
            Name = "";
            Namespace = "";
            Author = "";
            this.Description = "";
            this.VersionString = "0.0.0.0";
        }

        /// <summary>
        /// 应用程序名称
        /// </summary>
        [XmlElement("name")]
        public string Name { get;  set; }
        
        /// <summary>
        /// 程序版本号。遵从.Net版本规则
        /// </summary>
        [XmlElement("version")]
        public string VersionString { get;  set; }
        [NonSerialized]
        private Version m_Version;
        [XmlIgnore]
        public Version Version
        {
            get
            {
                if (m_Version == null)
                    m_Version = new Version(VersionString);
                return m_Version;
            }
        }
        /// <summary>
        /// 应用程序详细说明
        /// </summary>
        [XmlElement("description")]
        public string Description { get;  set; }

        /// <summary>
        /// 应用程序根命名空间
        /// </summary>
        [XmlElement("namespace")]
        public string Namespace { get;  set; }

 
        [XmlElement("options")]
        public AppOptions Options { get;  set; }
        /// <summary>
        /// 应用程序配置信息
        /// </summary>
        [Serializable]
        public class AppOptions
        {
            public AppOptions()
            {
                SyncSchemaToDB = true;
                GenerateDefaultMethos = true;
                GenerateIntegerId = true;
                IntegerIdDataType = "int";
                GenerateIdAndPK = true;
                UpgradeTransactionModel = true;
                DbProvider = "";
                DataSource = "";
                InstallMethod = "";
                UninstallMethod = "";

            }
            
            /// <summary>
            /// 数据库驱动名称
            /// </summary>
            [XmlElement("dbProvider")]
            public string DbProvider { get; set; }

            /// <summary>
            /// 数据源配置
            /// </summary>
            [XmlElement("dataSource")]
            public string DataSource { get; set; }

            [XmlElement("syncSchemaToDB")]
            public bool SyncSchemaToDB { get;  set; }

            [XmlElement("generateDefaultMethos")]
            public bool GenerateDefaultMethos { get; set; }

            /// <summary>
            /// 是否生成默认的ID和PK约束
            /// 当前版本固定生成
            /// </summary>
            [XmlElement("generateIdAndPK")]
            public bool GenerateIdAndPK { get; set; }

            /// <summary>
            /// 是否提升事务级别
            /// </summary>
            [XmlElement("upgradeTransactonModel")]
            public bool UpgradeTransactionModel { get; set; }

            /// <summary>
            /// 执行安装的方法。在同步schema后执行。
            /// 引用service中的方法，语法为:$(service).method
            /// 签名为:void install(Version ver,Version lastVer)
            /// </summary>
            [XmlElement("installMethod")]
            public string InstallMethod { get; set; }

            /// <summary>
            /// 执行反安装的方法。
            /// 引用service中的方法，语法为:$(service).method
            /// 签名为:void install(Version ver,Version lastVer)
            /// </summary>
            [XmlElement("uninstallMetod")]
            public string UninstallMethod { get; set; }

            /// <summary>
            /// 是否使用独立的数据库
            /// </summary>
            [XmlElement("useStandaloneDatabase")]
            public bool UseStandaloneDatabase { get; set; }

            /// <summary>
            /// 是否自动生成整型的主键
            /// </summary>
            [XmlElement("generateIntegerId")]
            public bool GenerateIntegerId { get; set; }

            /// <summary>
            /// 自动生成的主键类型
            /// </summary>
            [XmlElement("integerIdDataType")]
            public string IntegerIdDataType { get; set; }
        }

        /// <summary>
        /// 实体定义文件集
        /// </summary>
        [XmlArrayItem("entity")]
        [XmlArray("entities")]
        public string[] EntityDefinitions { get; set; }

        /// <summary>
        /// 服务定义文件集
        /// </summary>
        [XmlArrayItem("service")]
        [XmlArray("services")]
        public string[] ServiceDefinitions { get; set; }

        /// <summary>
        /// 实体定义集合
        /// </summary>
        [XmlIgnore]
        public IList<Entity> Entities { get;  set; }

        /// <summary>
        /// 服务定义集合
        /// </summary>
        [XmlIgnore]
        public IList<Service> Services { get;  set; }

        /// <summary>
        /// 作者
        /// </summary>
        [XmlElement("author")]
        public string Author { get; set; }

        [XmlArray("assemblies")]
        [XmlArrayItem("assembly")]
        public string[] Assemblies { get;  set; }

        [XmlArray("imports")]
        [XmlArrayItem("import")]
        public string[] Imports { get; set; }
    }

    
}
