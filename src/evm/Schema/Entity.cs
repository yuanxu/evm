﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.Schema
{
    /// <summary>
    /// 实体定义
    /// </summary>
    [XmlRoot("entity", Namespace = "http://evm.org/entity")]
    [Serializable]
    public class Entity
    {
        
        public Entity()
        {
            Statements=new List<Statement>();
        }
        /// <summary>
        /// 实体名
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        /// 实体表名
        /// </summary>
        [XmlAttribute("dbName")]
        public string DbName { get; set; }

        /// <summary>
        /// 实体属性
        /// </summary>
        [XmlArrayItem("property",Type=typeof(Property))]
        [XmlArray("properties")]
        public List<Property> Properties { get; set; }

        /// <summary>
        /// 缓存模型
        /// </summary>
        [XmlArrayItem("cacheModel",Type=typeof(CacheModel))]
        [XmlArray("cacheModels")]
        public List<CacheModel> CacheModels { get; set; }

        /// <summary>
        /// 索引定义
        /// </summary>
        [XmlArrayItem("index",Type=typeof(Index))]
        [XmlArray("indexes")]
        public List<Index> Indexes { get; set; }

        [XmlIgnore]
        public IList<Statement> Statements { get; private set; }

        /// <summary>
        /// 所属App
        /// </summary>
        [XmlIgnore]
        public App App { get;  set; }

        /// <summary>
        /// 实体定义文件名
        /// </summary>
        [XmlIgnore]
        public string FileName { get; internal set; }

        /// <summary>
        /// 实体友好名字
        /// </summary>
        [XmlAttribute("text")]
        public string Text { get; set; }

        /// <summary>
        /// 是否是枚举类型
        /// </summary>
        [XmlAttribute("isEnum")]
        public bool IsEnum { get; set; }
    }

    public static class EntityHelper
    {
        /// <summary>
        /// 根据实体名获取实体数据库名
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="entityName"></param>
        /// <returns></returns>
        public static string GetEntityDbNameByName(this IList<Entity> entities ,string entityName)
        {
        if (entities == null)
                return "";
            var vv = entities.Where(ety => ety.Name == entityName);
            if (vv.Count() > 0)
                return vv.First().DbName;
            else
                return "";
        }


        /// <summary>
        /// 根据实体名获取实体定义
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="entityName"></param>
        /// <returns></returns>
        public static Entity GetEntityByName(this IList<Entity> entities, string entityName)
        {
            if (entities == null)
                return null;
            var vv = entities.Where(ety => ety.Name == entityName);
            if (vv.Count() > 0)
                return vv.First();
            else
                return null;
        }

        /// <summary>
        /// 判断实体是否是枚举。如果未找到实体也返回false
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="entityName"></param>
        /// <returns></returns>
        public static bool IsEnum(this IList<Entity> entities,string entityName)
        {
            var entity = GetEntityByName(entities, entityName);
            if (entity == null)
                return false;
            return entity.IsEnum;
        }
    }
}
