﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.Schema
{
    /// <summary>
    /// 缓存模型
    /// </summary>
    [Serializable]
    public class CacheModel
    {

        /// <summary>
        /// 唯一标识
        /// </summary>
        [XmlAttribute("id")]
        public string ID { get; set; }

        /// <summary>
        /// 是否序列化
        /// </summary>
        [XmlAttribute("serialize")]
        public string Serialize { get;set; }

        /// <summary>
        /// 是否只读
        /// </summary>
        [XmlAttribute("readOnly")]
        public string ReadOnly { get; set; }

        /// <summary>
        /// 缓存实现方式
        /// </summary>
        [XmlAttribute("implementation")]
        public CacheModelImplementation Implementation { get; set; }

        [XmlElement("flushOnExecute")]
        public FlushOnExecute[] FlushOnExecutes{ get; set; }

        [XmlElement("flushInterval")]
        public FlushInterval FlushInterval { get; set; }

        [XmlElement("property")]
        public CacleModelProperty[] Properites { get; set; }
    }
    
    #region CacheModel辅助声明
        
    [Serializable]
    public enum CacheModelImplementation
    {
        LRU,
        MEMORY,
        FIFO
    }

    /// <summary>
    /// 缓存间隔
    /// </summary>
    [Serializable]
    public class FlushInterval
    {
        /// <summary>
        /// 毫秒
        /// </summary>
        [XmlAttribute("milliseconds")]
        public byte MilliSeconds { get; set; }

        /// <summary>
        /// 秒
        /// </summary>
        [XmlAttribute("seconds")]
        public byte Seconds { get; set; }

        /// <summary>
        /// 分
        /// </summary>
        [XmlAttribute("minutes")]
        public byte Minutes { get; set; }

        /// <summary>
        /// 时
        /// </summary>
        [XmlAttribute("hours")]
        public byte Hours { get; set; }
    }

    /// <summary>
    /// 执行时刷新
    /// </summary>
    [Serializable]
    public class FlushOnExecute
    {
        /// <summary>
        /// 语句
        /// </summary>
        [XmlAttribute("statement")]
        public string Statement { get; set; }
    }

    [Serializable]
    public class CacleModelProperty
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }
    }
    #endregion

}
