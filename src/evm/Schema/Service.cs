﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Evm.Schema
{
    [XmlRoot("service", Namespace = "http://evm.org/service")]
    [Serializable]
    public class Service
    {
        public Service()
        {
            ReferencedEntites = new List<Entity>();
            ReferencedServices = new List<Service>();
            Imports = new string[0];
        }
        /// <summary>
        /// 服务名
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        /// 服务参数
        /// </summary>
        [XmlArrayItem(ElementName="method",Type=typeof(Method))]
        [XmlArray(ElementName="methods")]
        public List<Method> Methods { get; set; }

        /// <summary>
        /// 直接引用(派生)的实体名
        /// </summary>
        [XmlAttribute("derivedEntity")]
        public string DerivedEntityName { get; set; }

        /// <summary>
        /// 直接引用（派生）的实体类
        /// </summary>
        [XmlIgnore]
        public Entity DerivedEntity
        {
            get
            {
                if (string.IsNullOrEmpty(DerivedEntityName))
                    return null;
                var result = this.App.Entities.Where(e => e.Name == DerivedEntityName);
                if (result == null || result.Count() == 0)
                    return null;
                else
                    return result.First();
            }
        }
        /// <summary>
        /// 是否根据DerivedEntity生成对应的语句
        /// </summary>
        [XmlAttribute("generateMethodsAccordingToDerivedEntity")]
        public bool GenerateMethodsAccordingToDerivedEntity { get;set; }

        /// <summary>
        /// 引用的实体
        /// </summary>
        [XmlIgnore]
        public IList<Entity> ReferencedEntites { get; private set; }

        /// <summary>
        /// 引用的服务
        /// </summary>
        [XmlIgnore]
        public IList<Service> ReferencedServices { get; private set; }

        [XmlIgnore]
        public string FileName { get; set; }

        [XmlIgnore]
        public App App { get; set; }

        [XmlArray("imports")]
        [XmlArrayItem("import")]
        public string[] Imports { get;  set; }

        /// <summary>
        /// 获取最终的接口名
        /// </summary>
        [XmlIgnore()]
        public string InterfaceName
        {
            get {return string.IsNullOrEmpty(DerivedEntityName) ? Name : DerivedEntityName; }

        }

        /// <summary>
        /// 服务约束
        /// </summary>
        [XmlAttribute("serviceContract")]
        public ServiceContract ServiceContract { get; set; }

        /// <summary>
        /// 服务描述
        /// </summary>
        [XmlAttribute("summary")]
        public string Summary { get; set; }
    }

    /// <summary>
    /// 服务约束
    /// </summary>
    [Serializable]
    public enum ServiceContract
    {
        /// <summary>
        /// 公共服务，提供接口定义
        /// </summary>
        Public,

        /// <summary>
        /// 私有服务，一般用于服务内部的工具类等
        /// </summary>
        Private,

        /// <summary>
        /// 私有对象，实现Sington模式的私有类
        /// </summary>
        PrivateObject,
    }
}
