﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Schema
{
    /// <summary>
    /// XML架构错误
    /// </summary>
    public class SchemaException:EvmException
    {
        public SchemaException(string message) : base(message) { }
        public SchemaException(string message, Exception inner) : base(message, inner) { }
    }
}
