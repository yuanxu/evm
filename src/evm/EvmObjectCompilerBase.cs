﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Evm.Schema;

namespace Evm
{
    /// <summary>
    /// 对象编译器基类
    /// </summary>
    public abstract class EvmObjectCompilerBase
    {
        /// <summary>
        /// 执行编译
        /// </summary>
        /// <param name="t">待编译对象</param>
        /// <returns>编译后文本</returns>
        protected abstract string DoCompile(object t);

        /// <summary>
        /// 编译
        /// </summary>
        /// <param name="t"></param>
        /// <param name="options"></param>
        public virtual void Compile(object t)
        {
            BeginCompile(t);
            var code = DoCompile(t);
            EndCompile(t, code);
            OnCompiledEvmObject(t, code);
        }

        public virtual  void Setup(App app,string outputDir,string tempDir)
        {
            App = app;
            OutputDir = outputDir;
            TempDir = tempDir;
        }

        /// <summary>
        /// 开始编译
        /// </summary>
        /// <param name="t"></param>
        protected virtual void BeginCompile(object t)
        {

        }
        /// <summary>
        /// 结束编译
        /// </summary>
        /// <param name="t"></param>
        /// <param name="code"></param>
        protected virtual void EndCompile(object t,string code)
        {}
        /// <summary>
        /// 是否是实体编译器
        /// </summary>
        public abstract bool IsEntityCompiler { get; }

        /// <summary>
        /// 是否是服务编译器
        /// </summary>
        public abstract bool IsServiceCompiler { get; }

        /// <summary>
        /// 正在编译的App
        /// </summary>
        public App App { get; private set; }

        /// <summary>
        /// 临时目录
        /// </summary>
        public string TempDir { get; private set; }

        /// <summary>
        /// 最终输出目录
        /// </summary>
        public string OutputDir { get; set; }

        /// <summary>
        /// 已完成编译事件。没完成一个对象编译触发一次
        /// </summary>
        public event EvmObvjectCompilerEventHandler Compiled;
        private void OnCompiledEvmObject(object source, string target)
        {
            if (Compiled != null)
                Compiled(this, new EvmObjectCompilerEventArgs() { Source = source, Target = target });
        }

        /// <summary>
        /// 所有对象编译器执行完毕。即已经完成所有对象编译工作
        /// </summary>
        public event EventHandler CompiledAll;

        internal void OnCompileAll()
        {
            if (CompiledAll != null)
                CompiledAll(this, new EventArgs());
        }

        /// <summary>
        /// 主编译器
        /// </summary>
        public CompilerBase2 MainCopmiler { get; set; }

        private List<string> targetFiles = new List<string>();
        /// <summary>
        /// 编译好的目标文件
        /// </summary>
        public IList<string> TargetFiles { get { return targetFiles; } }

    }

    public class EvmObjectCompilerEventArgs : EventArgs
    {
        /// <summary>
        /// 编译目标
        /// </summary>
        public object Source { get; set; }

        /// <summary>
        /// 编译后的目标码
        /// </summary>
        public string Target { get; set; }

        /// <summary>
        /// 写入文件
        /// </summary>
        public void WriteToFile(string fileName) 
        {
            FileName = fileName;
            Utils.TplUtil.WriteToFile(fileName, Target);
        }

        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName { get; set; }
    }

    public delegate void EvmObvjectCompilerEventHandler(object sender, EvmObjectCompilerEventArgs e);

    
}
