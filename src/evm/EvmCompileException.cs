﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm
{
    public class EvmCompileException:EvmException
    {
        public EvmCompileException()
            : base("编译错误！")
        { }
        public EvmCompileException(string message):base(string.Format("编译错误！\r\n{0}",message))
        {}
    }
}
