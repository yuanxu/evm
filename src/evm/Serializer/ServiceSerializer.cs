﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Evm.Schema;
using System.IO;
using System.Xml.Serialization;
using System.Collections;

namespace Evm.Serializer
{
    class ServiceSerializer
    {
        XmlSerializer sx = new XmlSerializer(typeof(Service));
        public Service Deserialize(App app,string fileName,StreamReader reader)
        {
            var svc = (Service)sx.Deserialize(reader.BaseStream);
            svc.App = app;
            svc.FileName = fileName;
            if (svc.GenerateMethodsAccordingToDerivedEntity)
                GenerateMethosaccordingToDerivedEntity(svc);
            
            return svc;
        }

        private void GenerateMethosaccordingToDerivedEntity(Service svc)
        {
            if (svc.DerivedEntity == null)
                throw new SchemaException(string.Format("服务定义，声明根据继承的实体生成方法，但未提供需要继承的实体。(服务:{0})",svc.Name));
            foreach (var stm in svc.DerivedEntity.Statements)
            {
                svc.Methods.Add(new Method()
                {
                    ID = stm.ID,
                    IsAutoGenerated = stm.IsAutoGenerated,
                    RefStatement = string.Format("$({0}).{1}", svc.DerivedEntityName, stm.ID)
                });
            }
        }

    }
   
}
