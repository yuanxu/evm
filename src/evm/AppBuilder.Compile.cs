﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using Evm.Schema;
using System.Diagnostics;

namespace Evm
{
    /// <summary>
    /// AppBuilder
    /// </summary>
    public sealed partial class AppBuilder
    {
     
        #region Signton
        private static AppBuilder m_instance;
        public static AppBuilder Instance
        {
            get
            {
                if (m_instance == null)
                {
                    lock (typeof(AppBuilder))
                    {
                        if (m_instance == null)
                            m_instance = new AppBuilder();
                    }
                }
                return m_instance;
            }
        }
        #endregion

       

        /// <summary>
        /// 编译应用
        /// </summary>
        /// <param name="fileName">app定义文件名</param>
        /// <param name="compilers">编译器集合</param>
        /// <param name="tempDir">中间临时目录</param>
        /// <param name="outDir">输出目录</param>
        public void Compile(string fileName, IEnumerable<CompilerBase2> compilers)
        {
            
            Debug.Assert(compilers != null);
            App app = Parse(fileName);
           
            foreach (var compiler in compilers)
            {
                log.Info("");
                log.Info(string.Format("正在启动编译器({0})进行编译...", compiler.Name));
                log.Info("");
                compiler.Setup(app, Evm.Configuration.Environment.Instance.OutputDir, Evm.Configuration.Environment.Instance.AppFolder + "\\Temp");
                compiler.Compile();
               /*var exitCode = compiler.Compile(app,tempDir,EvmLibDir,outDir,ci,dataProvider);
               if (exitCode != 0)
                   throw new EvmCompileException(compiler.FriendlyName);*/
            }
        }

       
        
    }
}
