﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Evm.Schema;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using Evm.Serializer;
using System.Xml;
using System.Xml.Schema;
using System.Reflection;
using log4net;
using Evm.Utils;

namespace Evm
{
    /// <summary>
    /// App实例解析/构建器
    /// </summary>
    public sealed partial class AppBuilder
    {
        private static ILog log = LogManager.GetLogger(typeof(AppBuilder));        
        /// <summary>
        /// 从xml文档解析App
        /// </summary>
        /// <param name="fileName">App配置元文件名</param>
        /// <returns></returns>
        public App Parse(string fileName)
        {
            //验证app.xml是否符合架构要求
            log.Info(string.Format("开始解析App[{0}].",fileName));
            Utils.TraceTime tt = new TraceTime(log);
            tt.ReInitWhenStatistic = true;
            try
            {
                SchemaUtils.Validate(fileName);
            }
            catch (XmlSchemaException e)
            {
                
                log.Error(string.Format("App[{0}]验证失败！行:{1}，消息:{2}",fileName,e.LineNumber,e.Message));
                throw ;
            }
           // tt.Statistic("验证Schema");

            App app = new App();
            XmlSerializer xs = new XmlSerializer(typeof(App));
            
            using (StreamReader reader = new StreamReader(fileName))
            {
                app = (App)xs.Deserialize(reader.BaseStream);
            }
            //tt.Statistic("反序列化App.xml");
            FileInfo fileInfo = new FileInfo(fileName);
            
            EntitySerializer es = new EntitySerializer();
            //逐个解析实体定义
            if (app.EntityDefinitions != null)
            {
                foreach (var item in app.EntityDefinitions)
                {
                    string name = fileInfo.DirectoryName + "\\" + item;
                    //验证文件是否符合架构要求
                    try
                    {
                        SchemaUtils.Validate(name);
                    }
                    catch (XmlSchemaException e)
                    {
                        log.Error(string.Format("验证[{0}]失败！行:{1} 消息:{2}", name, e.LineNumber, e.Message));
                        throw;
                    }

                    using (StreamReader reader = new StreamReader(name))
                    {
                        var entity = es.Deserialize(app,item, reader);
                        //验证&确保实体名不能重复
                        if ((from et in app.Entities where et.Name == entity.Name select et).Count() > 0)
                        {
                            throw new SchemaException("实体名不能重复！"+entity.Name);
                        }

                        app.Entities.Add(entity);
                    }
                }

            }
          //  tt.Statistic("解析实体");
            //逐个解析服务定义
            if (app.ServiceDefinitions != null)
            {
                ServiceSerializer xsSvc = new ServiceSerializer();
                foreach (var item in app.ServiceDefinitions)
                {
                    string name = fileInfo.DirectoryName + "\\" + item;
                    //验证文件是否符合架构要求
                    try
                    {
                        SchemaUtils.Validate(name);
                    }
                    catch (XmlSchemaException e)
                    {
                        log.Error(string.Format("验证[{0}]失败！行:{1} 消息:{2}", name, e.LineNumber, e.Message));
                        throw;
                    }
                    using (StreamReader reader = new StreamReader(name))
                    {
                        var svc = (Service)xsSvc.Deserialize(app,item,reader);
                        //验证&确保服务名不能重复
                        if ((from et in app.Services where  et.Name== svc.Name select et).Count() > 0)
                        {
                            throw new SchemaException("服务名不能重复！" + svc.Name);
                        }
                        app.Services.Add(svc);
                    }
                }
            }
          //  tt.Statistic("解析服务");
            //验证引用完整性
            app.Validate();
            // tt.Statistic("验证");
            #region 整理服务引用dao的参数调整

            foreach (var m in 
                    (from f_svc in app.Services 
                     from f_method in f_svc.Methods 
                     where !string.IsNullOrEmpty(f_method.RefStatement) 
                     select f_method))
            {
                //整理引用语句
                if (Utils.RefUtils.HasRef(m.RefStatement))
                {
                    var kv = Utils.RefUtils.GetSimpleRefPart(m.RefStatement);
                    var exp = from ett in app.Entities
                              from stm in ett.Statements
                              where stm.Entity.Name == kv.Key && stm.ID == kv.Value
                              select stm;
                    var statement = exp.First();
                    m.Parameters = statement.Parameters;
                    m.ResultClass = statement.ResultClass;
                    m.ListClass = statement.ListClass;
                    m.SupportPaging = statement.SupportPaging;
                    m.SupportSort = statement.SupportSort;
                    m.Summary = string.IsNullOrEmpty(m.Summary) ? statement.Summary : m.Summary;
                }
            }

            #endregion
            log.Info(string.Format("App[{0}]解析完毕.",fileName));
            return app;
        }
    }
}
