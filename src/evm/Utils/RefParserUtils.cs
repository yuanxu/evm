﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Evm.Utils
{
    /// <summary>
    /// 引用分析
    /// </summary>
    public class RefUtils
    {
        /// <summary>
        /// 是否具有对象引用
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool HasRef(string input)
        {
            return Regex.IsMatch(input,Constants.REF_MATCH_EXP);
        }
        /// <summary>
        /// 分析引用对象和方法
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Key:对象;Value 方法</returns>
        public static Dictionary<string,HashSet<string>> GetRefParts(string input)
        {
            //例子:输入$(Donor).GetList
            Regex regex = new Regex(Constants.REF_MATCH_EXP);
            MatchCollection matches = regex.Matches(input);
            Dictionary<string, HashSet<string>> r = new Dictionary<string, HashSet<string>>();

            if (matches.Count == 0)
                return r;
            foreach (Match match in matches)
            {
                //分开对象和方法
                var results = Regex.Split(match.Value, Constants.REF_SPLIT_EXP);
                if (r.ContainsKey(results[1]))
                {
                    if (!r[results[1]].Contains(results[2]))
                        r[results[1]].Add(results[2]);
                }
                else
                {
                    r.Add(results[1], new HashSet<string>() { results[2] });
                }
            }
            return r;
        }

        /// <summary>
        /// 获取简单的引用定义，用于只引用了一个对象的一个成员/属性
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static KeyValuePair<string,string> GetSimpleRefPart(string input)
        {
            var refDef = Utils.RefUtils.GetRefParts(input);
            if (refDef.Count == 0)
                return new KeyValuePair<string,string>();

            var refObj = refDef.Keys.First();
            var refProperty = refDef[refObj].First();
            return new KeyValuePair<string, string>(refObj, refProperty);
        }



        /// <summary>
        /// 替换掉$(xxx)为yyy
        /// </summary>
        /// <param name="input"></param>
        public static string Replace(string input,string source,string target)
        {
            return Regex.Replace(input,@"\$\("+source+@"\)",target);
        }

        /// <summary>
        /// 替换$(xxx)为xxx
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string Replace(string input)
        {
            Regex regex = new Regex(Constants.REF_MATCH_EXP);
            Match match = regex.Match(input);
            if (!match.Success)
                return input; //未匹配原样返回
            var results = Regex.Split(match.Value, Constants.REF_SPLIT_EXP);
            return string.Format("{0}.{1}", results[1], results[2]);
        }
    }
}
