﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using System.Xml.Linq;
using System.Xml;
using System.IO;

namespace Evm.Utils
{
    /// <summary>
    /// XML架构工具
    /// </summary>
    internal sealed class SchemaUtils
    {
        private SchemaUtils() { }
        private static XmlSchemaSet m_SchemaSet;
        static SchemaUtils()
        {
            m_SchemaSet = new XmlSchemaSet();
            using (XmlReader reader = XmlReader.Create(GetembeddedResource(Constants.ENTITY_SCHEMA_FILE)))
            {
                m_SchemaSet.Add(Constants.ENTITY_NAMESPACE, reader);
            }
            using (XmlReader reader = XmlReader.Create(GetembeddedResource(Constants.APP_SCHEMA_FILE)))
            {
                m_SchemaSet.Add(Constants.APP_NAMESPACE, reader);
            }
            using (XmlReader reader = XmlReader.Create(GetembeddedResource(Constants.SERVICE_SCHEMA_FILE)))
            {
                m_SchemaSet.Add(Constants.SERVICE_NAMESPACE, reader);
            }

        }
        private static Stream GetembeddedResource(string name)
        {
            return typeof(AppBuilder).Assembly.GetManifestResourceStream("Evm."+name);
        }

        /// <summary>
        /// 验证xml是否符合evm规范
        /// </summary>
        /// <param name="doc"></param>
        public static void Validate(XDocument doc)
        {
            doc.Validate(m_SchemaSet, null);
        }

        /// <summary>
        /// 验证xml是否符合evm规范
        /// </summary>
        /// <param name="doc"></param>
        public static void Validate(XmlDocument doc)
        {
            doc.Schemas=m_SchemaSet;
            doc.Validate((o, e) => { throw e.Exception; });
        }

        /// <summary>
        /// 验证xml是否符合evm规范
        /// </summary>
        /// <param name="fileName"></param>
        public static void Validate(string fileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);
            Validate(doc);
            
        }
    }
}
