﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Evm.Utils
{
    /// <summary>
    /// 杂项工具
    /// </summary>
    public static  class MiscUtil
    {
        /// <summary>
        /// 从变量声明分离出变量名
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string GetVarName(string item)
        {
           string type, varName;
            GetVars(item,out type,out varName);
            return varName;
        }

        /// <summary>
        /// 从变量声明分离出变量名
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static void GetVars(string item,out string type,out string varName)
        {
            string v = item.Substring(item.IndexOf(" ") + 1); //去掉类型，只取变量名
            type = item.Substring(0,item.IndexOf(" ")).Trim(); //类型名

            var loc = v.IndexOf("=");
            if (loc >= 0)
            {
                v = v.Substring(0, loc); //如果有默认值，则去掉默认值
            }
            varName =  v.Trim();
        }


        /// <summary>
        /// 拆分参数声明
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static IList<KeyValuePair<string,string>> GetParameters(string parameters)
        {
            if (string.IsNullOrEmpty(parameters))
                return null;
            var result = new List<KeyValuePair<string, string>>();
            foreach (var item in SplitParameters(parameters))
            {
                string type, varName;
                GetVars(item, out type, out varName);
                result.Add(new KeyValuePair<string, string>(type,varName));
            }
            return result;
        }

        /// <summary>
        /// 拆分参数
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static IList<string> SplitParameters(string parameters)
        {
            var list = new List<string>();
            //先以空白区分。
            // int id,IList<KeyValuePair<string,string>>  list
            //拆分为：
            //int
            //id,IList<KeyValuePair<string,string>>
            //list
            #region 使用正则分开各组。

            
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"([\w|>]\s+[\w])", options);
     
            var tmpList = new List<string>();
            var parts = regex.Split(parameters);
            var stuff = "";
            for (int i = 0; i < parts.Length - 1; i += 2)
            {
                tmpList.Add(stuff + parts[i] + parts[i + 1].Substring(0, parts[i + 1].Length - 1));
                stuff = parts[i + 1].Substring(parts[i + 1].Length - 1);
            }
            #endregion

            //之后tmpList存放有如下格式数据：
            //string   
            //userName , Role 
            //role,IList<int> 
            //organIDList ,  IList<KeyValuePair<int,bool>> 
            //accountIDList,IList<KeyValuePair<int,int>> 
            //最后一个变量名存放于parts[parts.Length-1]中,此变量的第一位存于parts[parts.Lenth-2].last()
            var type = tmpList[0];
            var name = parts[parts.Length-2].Last()+ parts[parts.Length - 1];
            for (var i = 1; i < tmpList.Count ; i++)
            {
                //拆分：上一个参数的形参名和后一个参数的类型
                var loc = tmpList[i].IndexOf(',');
                list.Add(string.Format("{0} {1}", type, tmpList[i].Substring(0, loc)));
                type = tmpList[i].Substring(loc + 1);//下一个参数的类型
            }
            //最后一个参数
            list.Add(string.Format("{0} {1}", type, name));
            return list;
        }
    }
}
