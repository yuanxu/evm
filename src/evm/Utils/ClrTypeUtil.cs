﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Utils
{
    public static class  ClrTypeUtil
    {
        private static readonly Dictionary<string, Type> m_dict;
        static ClrTypeUtil()
        {
            m_dict = new Dictionary<string, Type>();
            m_dict.Add("string",typeof(string));

            m_dict.Add("byte", typeof(byte));
            m_dict.Add("sbyte", typeof(sbyte));
            
            m_dict.Add("short", typeof(short));
            m_dict.Add("int",typeof(int));
            m_dict.Add("long", typeof(long));

            m_dict.Add("ushort", typeof(ushort));
            m_dict.Add("uint", typeof(uint));
            m_dict.Add("ulong", typeof(ulong));

            m_dict.Add("float", typeof(float));
            m_dict.Add("double", typeof(double));

            m_dict.Add("money", typeof(decimal));
            m_dict.Add("decimal", typeof(decimal));

            m_dict.Add("datetime", typeof(DateTime));

        }
        public static Type GetType(string dataType)
        {
            var key = dataType.ToLower();
            if (m_dict.ContainsKey(key))
                return m_dict[key];
            else
                return null;
        }

        
    }
}
