﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Utils
{
    /// <summary>
    /// 常量定义
    /// </summary>
    internal class Constants
    {
        public const string APP_NAMESPACE = "http://evm.org/app";
        public const string ENTITY_NAMESPACE = "http://evm.org/entity";
        public const string SERVICE_NAMESPACE = "http://evm.org/service";

        public const string APP_SCHEMA_FILE = "AppDef.xsd";
        public const string ENTITY_SCHEMA_FILE = "EntityDef.xsd";
        public const string SERVICE_SCHEMA_FILE = "serviceDef.xsd";
        
        /// <summary>
        /// 用于匹配对象引用的正则
        /// </summary>
        public const string REF_MATCH_EXP = @"\$\(\w+\).\w+";

        /// <summary>
        /// 用于分开对象和方法调用的正则
        /// </summary>
        public const string REF_SPLIT_EXP = @"\$\(|\).";
    }
}
