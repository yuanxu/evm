﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.CSharp.RuntimeBinder;
namespace Evm.Utils
{
    /// <summary>
    /// Razor模板引擎工具
    /// </summary>
    public  class TplUtil
    {

        /// <summary>
        /// 从外部模板文件解析，并将结果写入新的文件<br />
        /// 默认从当前目录及上级目录的Template、Templates目录搜索给定的模板
        /// </summary>
        /// <param name="templateName">模板名</param>
        /// <param name="model">传给模板引擎的值对象</param>
        /// <param name="outFileName">输出文件的文件名</param>
        public static void Parse<T>(string templateName, T model, string outFileName)
        {
            if(!compiledTemplates.Contains(templateName.GetHashCode()))
            {
                Compile(ReadFromFile(templateName), model.GetType(),templateName);
            }
            ParseToFile<T>(templateName, model, outFileName);
        }

        /// <summary>
        /// 从外部模板文件解析，并将结果写入新的文件<br />
        /// 默认从当前目录及上级目录的Template、Templates目录搜索给定的模板
        /// </summary>
        /// <param name="templateName">模板名</param>
        /// <param name="model">传给模板引擎的值对象</param>
        /// <param name="outFileName">输出文件的文件名</param>
        public static void Parse(string templateName, object model, string outFileName)
        {
            if (!compiledTemplates.Contains(templateName.GetHashCode()))
            {
                Compile(ReadFromFile(templateName), model.GetType(), templateName);
            }
            ParseToFile(templateName, model, outFileName);
        }

        /// <summary>
        /// 从外部模板文件解析<br />
        /// 默认从当前目录及上级目录的Template、Templates目录搜索给定的模板
        /// </summary>
        /// <param name="templateName">模板名</param>
        /// <param name="model">传给模板引擎的值对象</param>
        /// <returns>模板解析结果</returns>
        public static string Parse<T>(string templateName, T model)
        {
            //读取模板
            if (!compiledTemplates.Contains(templateName.GetHashCode()))
            {
                Compile(ReadFromFile(templateName), model.GetType(), templateName);
            }
            return DoParse<T>(templateName, model);
        }


        /// <summary>
        /// 从外部模板文件解析<br />
        /// 默认从当前目录及上级目录的Template、Templates目录搜索给定的模板
        /// </summary>
        /// <param name="templateName">模板名</param>
        /// <param name="model">传给模板引擎的值对象</param>
        /// <returns>模板解析结果</returns>
        public static string Parse(string templateName, object model)
        {
            //读取模板
            if (!compiledTemplates.Contains(templateName.GetHashCode()))
            {
                Compile(ReadFromFile(templateName), model.GetType(), templateName);
            }
            return DoParse(templateName, model);
        }

        /// <summary>
        /// 从嵌入式资源中加载模板并解析
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resourceName"></param>
        /// <param name="model"></param>
        /// <param name="outFileName"></param>
        public static void ParseFromResource<T>(string resourceName, T model, string outFileName)
        {
            if (!compiledTemplates.Contains(resourceName.GetHashCode()))
            {
                Compile(ReadFromResource(resourceName), model.GetType(), resourceName);
            }
            ParseToFile<T>(resourceName, model, outFileName);
        }

        /// <summary>
        /// 从嵌入式资源中加载模板并解析
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resourceName"></param>
        /// <param name="model"></param>
        /// <param name="outFileName"></param>
        public static void ParseFromeResource(string resourceName, object model, string outFileName)
        {
            if (!compiledTemplates.Contains(resourceName.GetHashCode()))
            {
                Compile(ReadFromResource(resourceName), model.GetType(), resourceName);
            }
            ParseToFile(resourceName, model, outFileName);
        }

        /// <summary>
        /// 从嵌入式资源中加载模板并解析
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resourceName"></param>
        /// <param name="model"></param>
        /// <param name="outFileName"></param>
        public static string  ParseFromResource<T>(string resourceName, T model)
        {
            if (!compiledTemplates.Contains(resourceName.GetHashCode()))
            {
                Compile(ReadFromResource(resourceName), model.GetType(), resourceName);
            }
            return DoParse<T>(resourceName, model);
        }


        /// <summary>
        /// 从嵌入式资源中加载模板并解析
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resourceName"></param>
        /// <param name="model"></param>
        /// <param name="outFileName"></param>
        public static string ParseFromResource(string resourceName, object model)
        {
            if (!compiledTemplates.Contains(resourceName.GetHashCode()))
            {
                Compile(ReadFromResource(resourceName),resourceName);
            }
            return DoParse(resourceName, model);
        }

        private static void Compile(string template,string templateName)
        {
            try
            {
                RazorEngine.Razor.Compile(template, templateName);
            }
            catch (RazorEngine.Templating.TemplateCompilationException compilationException)
            {
                log4net.ILog log = log4net.LogManager.GetLogger(typeof(TplUtil));
                foreach (var error in compilationException.Errors)
                {
                    log.Error(error.ErrorText);
                }
                throw;
            }
        }
        private static void Compile(string template,Type type, string templateName)
        {
            try
            {
                RazorEngine.Razor.Compile(template, type, templateName);
            }
            catch (RazorEngine.Templating.TemplateCompilationException compilationException)
            {
                log4net.ILog log = log4net.LogManager.GetLogger(typeof(TplUtil));
                foreach (var error in compilationException.Errors)
                {
                    log.Error(error.ErrorText);
                }
                throw;
            }
        }

        private static void ParseToFile<T>(string template, T model, string outFileName)
        {
            string result =DoParse<T>(template, model);
            WriteToFile(outFileName, result);
        }

        private static void ParseToFile(string template, object model, string outFileName)
        {
            string result = DoParse(template, model);
            WriteToFile(outFileName, result);
        }
        private static HashSet<int> compiledTemplates = new HashSet<int>();

        private static string DoParse<T>(string template, T model)
        {
            try
            {
                return RazorEngine.Razor.Run<T>(model, template);
            }

            catch (RazorEngine.Templating.TemplateCompilationException compilationException)
            {
                log4net.ILog log = log4net.LogManager.GetLogger(typeof(TplUtil));
                foreach (var error in compilationException.Errors)
                {
                    log.Error(error.ErrorText);
                }
                throw;
            }
        }
        private static string  DParse(string template,object model)
        {
            try
            {
                return RazorEngine.Razor.Run( model,template);
            }
            catch (RazorEngine.Templating.TemplateCompilationException compilationException)
            {
                log4net.ILog log = log4net.LogManager.GetLogger(typeof(TplUtil));
                foreach (var error in compilationException.Errors)
                {
                    log.Error(error.ErrorText);
                }
                throw;
            }
        }

        /// <summary>
        /// 写入文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="content"></param>
        public static void WriteToFile(string fileName, string content)
        {
            using (StreamWriter writer = new StreamWriter(fileName, false, Encoding.UTF8))
            {
                writer.Write(content);
            }
        }


        private static string ReadFromFile(string templateName)
        {
            var searchDir = new HashSet<string>() { "Template\\{0}.cshtml", "Templates\\{0}.cshtml", "..\\Template\\{0}.cshtml", "..\\Templates\\{0}.cshtml" };
            foreach (var item in searchDir)
            {
                var file = string.Format(item, templateName);
                if (File.Exists(file))
                {
                    using (StreamReader reader = new StreamReader(file))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
            throw new FileNotFoundException(string.Format("未找到模板{0}.cshtml", templateName));
        }

        public static string ReadFromResource(string resourceName)
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    using (StreamReader reader = new StreamReader(assembly.GetManifestResourceStream(resourceName),Encoding.UTF8))
                    {
                        return reader.ReadToEnd();
                    }
                }
                catch
                { }
            }
            throw new FileNotFoundException(string.Format("未找到模板{0}", resourceName));
        }
    }
}
