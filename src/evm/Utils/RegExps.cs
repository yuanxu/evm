﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Utils
{
    /// <summary>
    /// 内建正则表达式
    /// </summary>
    public sealed class RegExps
    {
        private static Dictionary<string,string> m_Dict = new Dictionary<string,string>();
        private RegExps() {}
        static RegExps()
        {
            m_Dict.Add("mobile","");
            m_Dict.Add("email",@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            m_Dict.Add("url",@"[a-zA-z]+://[^\s]*");
            m_Dict.Add("qq",@"[1-9][0-9]{4,}");
            m_Dict.Add("zip",@"[1-9]\d{5}(?!\d)");
            m_Dict.Add("chinesecharater",@"[\u4e00-\u9fa5]");
            m_Dict.Add("phone", @"((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)");
        }
        /// <summary>
        /// 过滤正则表达式，如果包含内置表达式则用内置表达式置换
        /// </summary>
        /// <param name="exp"></param>
        /// <returns></returns>
        public static string Filter(string exp)
        {
            var lowerExp = exp.ToLower();
            if (m_Dict.ContainsKey(lowerExp))
                return m_Dict[lowerExp];
            else
                return exp;
        }
    }
}
