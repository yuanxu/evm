﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace Evm.Utils
{
    /// <summary>
    /// 跟随执行时间
    /// </summary>
   public class  TraceTime
    {
       public TraceTime(Type t)
       {
           Init(t, null);
       }
       public TraceTime(ILog log)
       {
           Init(null, log);
       }
       /// <summary>
       /// 被跟踪程序名
       /// </summary>
       
       private DateTime ts;
       private ILog log;
       /// <summary>
       /// 输出统计信息后是否重置计数时间。默认否
       /// </summary>
       public bool ReInitWhenStatistic { get; set; }

       private void Init(Type t,ILog log = null)
       {
        
           ts = DateTime.Now;
           if (log == null)
           {
               this.log = LogManager.GetLogger(t);
           }
           else
           {
               this.log = log;
           }
           ReInitWhenStatistic = false;
       }

       public void Init()
       {
           ts = DateTime.Now;
       }

       public void Statistic(string str=null)
       {
           DateTime n = DateTime.Now;
           log.Info(string.Format("到目前[{1}]消耗时间:{0}", (n - ts).ToString(),str));
           if (ReInitWhenStatistic)
               Init();
       }
    }
}
