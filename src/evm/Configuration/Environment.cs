﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Web;

namespace Evm.Configuration
{
    /// <summary>
    /// 配置类
    /// </summary>
    public class Environment : ConfigurationSection
    {
       /// <summary>
       /// 应用程序(定义)存放目录
       /// </summary>
       [ConfigurationProperty("appFolder",DefaultValue=@"D:\Codes\evm\src\bin\Debug\Apps")]
       public string AppFolder
       {
           get
           {
               return this["appFolder"].ToString();
           }
           set 
           {
               this["appFolder"] = value;
           }
       }

       private string binDir;
        /// <summary>
        /// 应用程序bin/.dll目录地址
        /// </summary>
       public string BinDir
       {
           get
           {
               if (string.IsNullOrEmpty(binDir))
                   binDir = AppDomain.CurrentDomain.SetupInformation.PrivateBinPath == null ? (AppDomain.CurrentDomain.BaseDirectory == null ? System.Environment.CurrentDirectory : AppDomain.CurrentDomain.BaseDirectory)
                       : AppDomain.CurrentDomain.SetupInformation.PrivateBinPath;
               return binDir;
           }
           set { binDir = value; }
       }

       [ConfigurationCollection(typeof(CompilerCollection))]
       [ConfigurationProperty("RtlCompilers")]
       public CompilerCollection RtlCompilers
       {
           get
           {
               return (CompilerCollection)this["RtlCompilers"];
           }
       }

       /// <summary>
       /// 数据库架构(Schema)同步器
       /// </summary>
       [ConfigurationCollection(typeof(SchemaSyncerCollection))]
        [ConfigurationProperty("SchemaSyncers")]
       public SchemaSyncerCollection SchemaSyncers
       {
           get { return (SchemaSyncerCollection)this["SchemaSyncers"]; }
          
       }

      

       #region 单件模式

       private static Environment enviroment = null;

       /// <summary>
       /// 获取配置实例
       /// </summary>
       public static Environment Instance
       {
           get
           {

               if (enviroment == null)
               {
                   lock (typeof(Environment))
                   {
                       if (enviroment == null)
                       {
                           var sec = ConfigurationManager.GetSection("Evm");
                           if (sec != null)
                               enviroment = (Environment)sec;
                           else
                               enviroment = GetDefaultInstance();
                       }
                   }
               }
               return enviroment;
           }
       }

       private static Environment GetDefaultInstance()
       {
           var instance = new Environment();
           if (HttpContext.Current != null)
               instance.binDir = HttpContext.Current.Server.MapPath(".\\bin");
           else
                instance.binDir = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
           
           if (instance.AppFolder.LastIndexOf("\\") < 0)
           {
               instance.AppFolder += "\\";
           }
           instance.AppFolder = instance.BinDir + "\\Apps\\";
           return instance;
       }
       #endregion


       [ConfigurationProperty("outputDir", DefaultValue = @".")]
       public string OutputDir
       {
           get
           {
               return this["outputDir"].ToString();
           }
           set
           {
               this["outputDir"] = value;
           }
       }
    }
}
