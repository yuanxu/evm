﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Evm.Configuration
{
    public class CompilerCollection : ConfigurationElementCollection
    {
        public CompilerCollection()
        {
            BaseAdd(new CompilerElement() { Name = "scs", Type = "Evm.Compiler.Server.Core.CoreCompiler,Evm.scs2" });
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new CompilerElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CompilerElement)element).Name;
        }
    }
    public class CompilerElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return this["name"].ToString(); }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("type", IsRequired = true)]
        public string Type
        {
            get { return this["type"].ToString(); }
            set { this["type"] = value; }
        }
    }
}
