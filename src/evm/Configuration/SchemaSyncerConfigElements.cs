﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Evm.Configuration
{
    public class SchemaSyncerCollection : ConfigurationElementCollection
    {
        public SchemaSyncerCollection()
        {
            BaseAdd(new SchemaSyncerElement() { Name = "sqlServer2.0", Type = "Evm.Rtl.Sync.Provider.MsSqlSyncer,Evm.Rtl" });
            BaseAdd(new SchemaSyncerElement() { Name = "sqlServer2005", Type = "Evm.Rtl.Sync.Provider.MsSqlSyncer,Evm.Rtl" });
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new SchemaSyncerElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SchemaSyncerElement)element).Name;
        }
        public SchemaSyncerElement this[int index]
        {
            get
            {
                return (SchemaSyncerElement)BaseGet(index);
            }
        }
    }

    public class SchemaSyncerElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return this["name"].ToString(); }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("type", IsRequired = true)]
        public string Type
        {
            get { return this["type"].ToString(); }
            set { this["type"] = value; }
        }
    }
}
