﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Evm.Schema;
using log4net;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Evm
{
    /// <summary>
    /// 编译器基类
    /// </summary>
    public abstract class CompilerBase
    {
        public string CscDir { get; set; }

        /// <summary>
        /// 执行编译动作
        /// </summary>
        /// <param name="app">解析后的应用程序</param>
        /// <param name="workDir">工作目录</param>
        /// <param name="libDir">外部库路径</param>
        /// <param name="ci">编译器直接指令</param>
        public virtual int Compile(App app,string workDir,string libDir,string outDir,string ci,string dataProvider = "sqlServer2.0")
        {
            if (!Directory.Exists(outDir))
                Directory.CreateDirectory(outDir);
            return 0;
        }

        ILog log;

        public ILog Log
        {
            get
            {
                if(log==null)
                    log = LogManager.GetLogger(this.GetType());
                return log;
            }
        }

        protected int RunCsc(string cmdLine,string workDir)
        {
           // System.Environment.CurrentDirectory = workDir;
            using (Process process = new System.Diagnostics.Process())
            {
                process.StartInfo.FileName = string.Format(@"{0}\csc",CscDir);
                process.StartInfo.Arguments = cmdLine;

               // Logger.Info(string.Format("{0} {1}",process.StartInfo.FileName,cmdLine));

#if DEBUG
                Debug.Print( string.Format("{0} {1}", process.StartInfo.FileName, cmdLine));
#endif

                // 必须禁用操作系统外壳程序  
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.WorkingDirectory = workDir;
                // 为异步获取订阅事件  
                process.OutputDataReceived += new DataReceivedEventHandler(process_OutputDataReceived);
               
                process.Start();
                //var s = process.StandardOutput.ReadToEnd();
                
                // 异步获取命令行内容  
                process.BeginOutputReadLine();
                process.WaitForExit();
                return process.ExitCode;
            }  
        }

        private void process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            // 这里仅做输出的示例，实际上您可以根据情况取消获取命令行的内容  
            // 参考：process.CancelOutputRead()  
            if (!string.IsNullOrEmpty(e.Data) && e.Data.IndexOf("Microsoft")<0 && e.Data.IndexOf("版权所有")<0)
            {
                Log.Info(e.Data);
#if DEBUG
                Debug.Print(e.Data);
#endif
            }
        }

        /// <summary>
        /// 编译器友好名称
        /// </summary>
        public abstract string FriendlyName { get; }

        /// <summary>
        /// 编译器名称
        /// </summary>
        public abstract string Name { get;  }

        /// <summary>
        /// 提供编译器的附加信息
        /// </summary>
        public virtual string Information { get { return ""; } }

        /// <summary>
        /// 写入文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="content"></param>
        protected void WriteToFile(string fileName,string content)
        {
            Utils.TplUtil.WriteToFile(fileName, content);
        }

        
    }
}
