﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;


namespace Evm.Rtl.Test
{
 
    [TestFixture]
    public class EngineTest
    {

        [TestCase]
        public void  UploadApp()
        {
            Engine.Instance.UploadApp(@"Sample\app.xml");
        }

        [TestCase]
        public void LoadApp()
        {
            Engine.Instance.Load("NorthWindSample");
            var result = Engine.Instance.Invoke("Sample.NorthWind.CategorySrv", "GetByID", new string[] { "2" });
            
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(result.Data);
            var saveResult =  Engine.Instance.Invoke("Sample.NorthWind.CategorySrv", "Save", new string[] { json });

        }

        [TestCase]
        public void RazorTest()
        {
           // bool loaded = typeof(Microsoft.CSharp.RuntimeBinder.Binder).Assembly != null;
            string template = "Hello @Model.Name! Welcome to Razor!";
            string result = RazorEngine.Razor.Parse(template, new { Name = "World" });
            Console.WriteLine(result);
            Console.Read(); 
        }
    }
}
