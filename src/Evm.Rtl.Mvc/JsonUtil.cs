﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace Evm.Rtl.Web
{
    public static class JsonUtil
    {
        /// <summary>
        /// 获取EasyUI 格式的Json格式数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static void WriteJson<T>(Evm.Rtl.PagedList<T> list)
        {
            Write(list.ToJson("yyyy-MM-dd"));
        }
        public static void WritePagedJson<T>(Evm.Rtl.PagedList<T> list)
        {
            Write(list.ToPagedJson("yyyy-MM-dd"));
        }
        private static void Write(string value)
        {
            var Response = HttpContext.Current.Response;
            Response.Clear();
            //Response.ContentType = "application/json; charset=utf-8";
            Response.Write(value);
           //Response.End();
        }
        public static void WriteJson<T>(IList<T> list)
        {
            Write(list.ToJson("yyyy-MM-dd"));
        }
        public static void WritePagedJson<T>(IList<T> list)
        {
            Write(list.ToPagedJson("yyyy-MM-dd"));
        }
        public static void WriteJson(string value)
        {
            Write(value);
        }
        public  static void WriteSuccessReponse()
        {
            WriteJson(new InvokeResult());
        }
        public static void WriteJson(object value)
        {
            if (value is EvmException)
            {
                Write(JsonConvert.SerializeObject( new InvokeResult((value as EvmException).Message)));
            }
            else if (value is Exception)
            {
                Write(JsonConvert.SerializeObject(new InvokeUnknownException((value as Exception).Message)));
            }
            else
            {
                EvmDateTimeConverter isoDateTimeConverter = new EvmDateTimeConverter();

                isoDateTimeConverter.DateTimeFormat = "yyyy-MM-dd hh:mm:ss";
                Write(JsonConvert.SerializeObject(value, Formatting.Indented, isoDateTimeConverter));
            }
        }
    }
}
