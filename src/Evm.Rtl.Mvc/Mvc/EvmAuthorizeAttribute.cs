﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;

namespace Evm.Rtl.Mvc
{
    public class EvmAuthorizeAttribute : AuthorizeAttribute
    {
       
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool result = false;
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            if (!httpContext.User.Identity.IsAuthenticated)
                return false;

            string[] users = Users.Split(',');
            string[] roles = Roles.Split(',');
            var user = httpContext.User;
            if (users.Count(u => u == user.Identity.Name) > 0)
            {
                result = true;
            }
            else
            {
                foreach (var role in roles)
                {
                    if (user.IsInRole(role))
                    {
                        result = true;
                        break;
                    }
                }
            }

            if (!result)
            {
                httpContext.Response.StatusCode = 403;
            }
            return result;

        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (filterContext.HttpContext.Response.StatusCode == 403)
            {
                filterContext.Result = new RedirectResult("~/Account/UnAuthorized");
            }
        }
    }
}
