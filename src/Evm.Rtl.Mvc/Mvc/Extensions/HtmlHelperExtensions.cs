﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web;

namespace Evm.Rtl.Mvc.Extensions
{
    public static class HtmlHelperExtensions
    {
        /// <summary>
        /// 注册fckeditor
        /// </summary>
        /// <returns></returns>
        public static MvcHtmlString RegisterFCKeditorScripts(this HtmlHelper htmlHelper)
        {
            var url = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            var fckEditorjs = string.Format(@"<script src=""{0}"" type=""text/javascript""></script>", url.Content("~/Scripts/fckeditor/fckeditor.js"));
            return MvcHtmlString.Create(fckEditorjs);
        }

        /// <summary>
        /// FCKEdtior编辑器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString FckEditorFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression)
        {
            var md = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var url = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            MvcHtmlString textAreaHtml = htmlHelper.TextAreaFor(expression);
            string toggleEditor = string.Format(@"<script type=""text/javascript"">var oFCKeditor = new FCKeditor('{0}') ;oFCKeditor.BasePath=""{1}"";  oFCKeditor.ReplaceTextarea() ;</script>", md.PropertyName
                , url.Content("~/Scripts/FCKEditor/"));

            return MvcHtmlString.Create(textAreaHtml + toggleEditor);
        }

        /// <summary>
        /// 文件上传
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="html"></param>
        /// <param name="expression"></param>
        /// <param name="fileExtNames"></param>
        /// <returns></returns>
         public static MvcHtmlString FileFor<TModel, TValue>(this HtmlHelper<TModel> html,Expression<Func<TModel, TValue>> expression,string fileExtNames)
        {
            var md =ModelMetadata.FromLambdaExpression(expression,html.ViewData);
            var tagBuilder = new TagBuilder("input");
            tagBuilder.Attributes.Add("type","file");
            tagBuilder.GenerateId(md.PropertyName);
            tagBuilder.Attributes.Add("name", md.PropertyName);
            return MvcHtmlString.Create( tagBuilder.ToString(TagRenderMode.Normal));
        }

        /// <summary>
        /// 日期控件
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="html"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
         public static MvcHtmlString DatePickerFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
         {
             var md = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
             
             var textHtml= html.TextBoxFor(expression);
             var scriptHtml =string.Format("<script>$(document).ready(function() {$('#{0}').datepicker();});</script>",md.PropertyName);
             return MvcHtmlString.Create(textHtml+scriptHtml);
         }

        /// <summary>
        /// 注册DatePicker
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <returns></returns>
         public static MvcHtmlString RegisterDatePickerScripts(this HtmlHelper htmlHelper)
         {
             var url = new UrlHelper(htmlHelper.ViewContext.RequestContext);
             var html = url.Content("<link href='~/content/jquery-ui.css' rel='stylesheet' type='text/css'/>"
  + " <script src='~/Scripts/jquery-ui.min.js'></script>"
  + "<script src='~/Scripts/jquery.ui.datepicker-zh-CN.js'></script>");
             return MvcHtmlString.Create(html);
         }
    }
}
