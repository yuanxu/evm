﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Evm.Rtl.Mvc
{
    public class QueryOptions
    {
        /// <summary>
        /// 排序字段
        /// </summary>
        public string Column { get; set; }

        private string m_direction;
        /// <summary>
        /// 方向
        /// </summary>
        public string Direction
        {
            get { return m_direction; }
            set
            {
                var direction = value.ToLower();
                if (direction == "ascending" || direction == "asc")
                    m_direction = "ASC";
                else if (direction == "descending" || direction == "desc")
                    m_direction = "DESC";
                else
                    m_direction = "ASC";
            }
        }

        /// <summary>
        /// 是否允许排序
        /// </summary>
        public bool SortEnabled
        {
            get
            {
                return !string.IsNullOrEmpty(Column) && !string.IsNullOrEmpty(Direction);
            }
        }

        /// <summary>
        /// 转换为SqlMap可是识别的参数
        /// </summary>
        /// <returns></returns>
        public Hashtable ToHashtable()
        {
            Hashtable ht = new Hashtable();
            if (SortEnabled)
            {
                ht.Add("_orderby_", Column);
                ht.Add("_direction_", Direction);
            }

            return ht;
        }
       /// <summary>
       /// 当前页号
       /// </summary>
       public int PageIndex { get; set; }

       /// <summary>
       /// 页数
       /// </summary>
       public int PageSize { get; set; }
       
         public QueryOptions()
            : this("", "", 1, 20)
        { }
        public QueryOptions(string column, string direction)
            : this(column, direction, 1, 20)
        { }
        public QueryOptions(int pageIndex, int pageSize)
            : this("", "asc", pageIndex, pageSize)
        { }
        public QueryOptions(string column, string direction, int pageIndex, int pageSize)
        {
            Column = column;
            Direction = direction;
            PageIndex = pageIndex;
            PageSize = pageSize;
        }

    }
}
