﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;


namespace Evm.Rtl.Web
{
    /// <summary>
    /// Evm handler运行时
    /// </summary>
    public class EvmHandler : IHttpHandler, IRequiresSessionState
    {
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// 处理请求
        /// 调用方式：Post
        /// 调用格式：type=?&method=&?arguments=?
        /// 返回:Json(InvokeResult)
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            var Request = context.Request;
            var Response = context.Response;
            var User = context.User;

            Response.ContentType = "application/json; charset=utf-8";
            var result = Engine.Instance.Invoke(
                (roles,users) =>
                {
                    if (!User.Identity.IsAuthenticated)
                        return false;
                    else
                    {
                        foreach (var role in roles.Split(new char[] { ',' }))
                        {
                            if (User.IsInRole(role))
                                return true;
                        }
                        foreach (var user in users.Split(new char[]{','}))
                        {
                            if (User.Identity.Name == user || user=="everyone")
                                return true;
                        }
                        return false;
                    }
                }
                , Request["type"], Request["method"], string.IsNullOrEmpty(Request["arguments"]) ? null : Request["arguments"].Split(new char[] { ',' }));

            
            Response.Write(result.ToJson());
        }
    }
}
