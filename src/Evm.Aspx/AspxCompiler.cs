﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Evm.Compiler.Aspx
{
    /// <summary>
    /// Asp.Net Compiler
    /// </summary>
    public class AspxCompiler:CompilerBase2
    {
        private ControllerCompiler controllerCompiler = new ControllerCompiler();
        private ListCompiler listCompiler = new ListCompiler();
        private EditorCompiler editorCompiler = new EditorCompiler();

        public AspxCompiler():base()
        {
            AddEvmObjectCompiler(controllerCompiler);
            AddEvmObjectCompiler(listCompiler);
            AddEvmObjectCompiler(editorCompiler);
        }
        public override string FriendlyName
        {
            get { return "Evm Asp.Net Compiler"; }
        }

        public override string Name
        {
            get { return "Aspx"; }
        }
        public override void Compile()
        {
            foreach (var svc in App.Services)
            {
                if (svc.DerivedEntity == null || svc.ServiceContract!= Schema.ServiceContract.Public )
                    continue;
                Log.InfoFormat("正在编译:{0}",svc.Name);
                controllerCompiler.Compile(svc);
                listCompiler.Service = svc;
                
                listCompiler.Compile(svc.DerivedEntity);
                editorCompiler.Compile(svc);
            }
            MiscCompile();

            Log.Info("执行完毕！");
        }

        public override void Setup(Schema.App app, string outputDir, string tempDir, Dictionary<string, string> configuration = null)
        {
            base.Setup(app, outputDir, tempDir, configuration);
           this.App = app;
           if (configuration != null)
               Configuration = configuration;
           this.OutputDir = Configuration==null || Configuration["Output"] == null ? outputDir : Configuration["Output"];
           foreach (var c in this.EvmObjectCompilers)
           {
               c.Setup(app, OutputDir, TempDir);
           }
        }

        /// <summary>
        /// 杂项文件生成
        /// </summary>
        private void MiscCompile()
        {
            Utils.TplUtil.WriteToFile(string.Format("{0}\\readme.txt", OutputDir), GetContent("Evm.Compiler.Aspx.Templates.Readme.cshtml"));
            if(!Directory.Exists(string.Format("{0}\\Style",OutputDir)))
                Directory.CreateDirectory(string.Format("{0}\\Style",OutputDir));
            Utils.TplUtil.WriteToFile(string.Format("{0}\\Style\\Style.ascx", OutputDir), GetContent("Evm.Compiler.Aspx.Templates.Style.cshtml"));
            if (!Directory.Exists(string.Format("{0}\\Script", OutputDir)))
                Directory.CreateDirectory(string.Format("{0}\\Script", OutputDir));
            Utils.TplUtil.WriteToFile(string.Format("{0}\\Script\\EvmUtils.js", OutputDir), GetContent("Evm.Compiler.Aspx.Templates.EvmUtils.js"));
        }

        private string GetContent(string p)
        {
            return Utils.TplUtil.ReadFromResource(p);
        }
    }
}
