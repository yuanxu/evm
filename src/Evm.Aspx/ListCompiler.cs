﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Evm.Compiler.Aspx
{
    public class ListCompiler:EvmObjectCompilerBase
    {
        protected override string DoCompile(object t)
        {
            return Utils.TplUtil.ParseFromResource<Schema.Service>("Evm.Compiler.Aspx.Templates.List.cshtml",Service);
        }
        public Schema.Service Service { get; set; }
        public override bool IsEntityCompiler
        {
            get { return true; }
        }

        public override bool IsServiceCompiler
        {
            get { return false; }
        }
        protected override void EndCompile(object t, string code)
        {
            
            var dir = string.Format("{0}\\{1}", OutputDir, (t as Schema.Entity).Name );
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            var fileName = string.Format("{0}\\{1}List.aspx", dir, (t as Schema.Entity).Name);
            Utils.TplUtil.WriteToFile(fileName, code);
        }
    }
}
