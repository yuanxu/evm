﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Evm.Compiler.Aspx
{
   public class ControllerCompiler:EvmObjectCompilerBase
    {
        protected override string DoCompile(object t)
        {
            return Utils.TplUtil.ParseFromResource<Schema.Service>("Evm.Compiler.Aspx.Templates.Controller.cshtml", t as Schema.Service);
        }

        public override bool IsEntityCompiler
        {
            get { return false; }
        }

        public override bool IsServiceCompiler
        {
            get { return true; }
        }
        protected override void EndCompile(object t, string code)
        {
            var svc = t as Schema.Service;
            var dir =string.Format("{0}\\{1}",OutputDir, svc.DerivedEntity == null ? svc.Name : svc.DerivedEntityName);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            var fileName = string.Format("{0}\\{1}Controller.aspx.cs", dir, svc.Name);
            Utils.TplUtil.WriteToFile(fileName, code);

            //写入空的页面类
            var pageContent = string.Format("<%@ Page Language='C#' AutoEventWireup='true' CodeBehind='{0}.aspx.cs' ClientIDMode='Static' Inherits='{1}' EnableViewState='false' %>"
                ,svc.Name+"Controller",svc.App.Namespace+".UI."+svc.DerivedEntityName+"."+svc.Name+"Controller");
            Utils.TplUtil.WriteToFile(string.Format("{0}\\{1}Controller.aspx",dir,svc.Name),pageContent);
        }
    }
}
