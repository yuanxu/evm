﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Evm.Compiler.Aspx
{
    /// <summary>
    /// 编辑器控件生成器
    /// </summary>
    public class EditorCompiler:EvmObjectCompilerBase
    {
        protected override string DoCompile(object t)
        {
            return Utils.TplUtil.ParseFromResource<Schema.Service>("Evm.Compiler.Aspx.Templates.Editor.cshtml", t as Schema.Service);
        }

        public override bool IsEntityCompiler
        {
            get { return true; }
        }

        public override bool IsServiceCompiler
        {
            get { return false; }
        }
        protected override void EndCompile(object t, string code)
        {
            var svc = t as Schema.Service;
            var dir = string.Format("{0}\\{1}", OutputDir, svc.DerivedEntity == null ? svc.Name : svc.DerivedEntityName);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            var fileName = string.Format("{0}\\{1}Editor.aspx", dir, svc.DerivedEntityName);
            Utils.TplUtil.WriteToFile(fileName, code);

        }
    }
}
