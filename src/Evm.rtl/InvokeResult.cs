﻿using System.Runtime.Serialization;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;


namespace Evm.Rtl
{
    /// <summary>
    /// Json Call result object
    /// </summary>
    public class InvokeResult
    {
        #region 构造函数

        /// <summary>
        /// 默认构造函数。成功，无数据
        /// </summary>
        public InvokeResult()
            : this(true, 0, "", null)
        {

        }

        /// <summary>
        /// 构造包含成功数据的返回对象
        /// </summary>
        /// <param name="data"></param>
        public InvokeResult(object data)
            : this(true, 0, "", data)
        { }

        /// <summary>
        /// 构造具有错误代码和错误消息的返回对象
        /// </summary>
        /// <param name="errorCode">错误代码</param>
        /// <param name="errorMessage">错误消息</param>
        public InvokeResult(int errorCode, string errorMessage)
            : this(false, errorCode, errorMessage, null)
        {

        }

        /// <summary>
        /// 构建具有错误代码的返回对象
        /// </summary>
        /// <param name="errorCode">错误代码</param>
        public InvokeResult(int errorCode)
            : this(errorCode, "")
        { }

        /// <summary>
        /// 构建具有错误消息的返回对象
        /// </summary>
        /// <param name="errorMessage">错误消息</param>
        public InvokeResult(string errorMessage)
            : this(int.MinValue, errorMessage)
        { }

        /// <summary>
        /// 构建返回对象
        /// </summary>
        /// <param name="isSucceed"></param>
        /// <param name="errorCode"></param>
        /// <param name="errorMessage"></param>
        /// <param name="data"></param>
        public InvokeResult(bool isSucceed, int errorCode, string errorMessage, object data)
        {
            IsSucceed = isSucceed;
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
            Data = data == null ? string.Format("") : data;
        }

        #endregion

        /// <summary>
        /// 获取或设置操作是否成功
        /// </summary>
        [DataMember]
        public bool IsSucceed { get; set; }

        /// <summary>
        /// 获取才做是否失败
        /// </summary>
        [DataMember]
        public bool IsError { get { return !IsSucceed; } }

        /// <summary>
        /// 获取或设置错误码
        /// </summary>
        [DataMember]
        public int ErrorCode { get; set; }

        /// <summary>
        /// 获取或设置错误消息
        /// </summary>
        [DataMember]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// 获取或设置实际结果数据
        /// </summary>
        [DataMember]
        public object Data { get; set; }

        public string ToJson()
        {
            //return AjaxPro.JavaScriptSerializer.Serialize(this);

            IsoDateTimeConverter isoDateTimeConverter = new IsoDateTimeConverter();

            isoDateTimeConverter.DateTimeFormat = "'yyyy'-'MM'-'dd' 'HH':'mm':'ss'";


            return JsonConvert.SerializeObject(this, Formatting.Indented, isoDateTimeConverter);
        }

    }

    /// <summary>
    /// 类型解析错误
    /// </summary>
    public class InvokeTypeResolveError : InvokeResult
    {
        public InvokeTypeResolveError()
            : base(-1, "Resolve Type Exception")
        { }
        public InvokeTypeResolveError(string type) : base(-1, string.Format("Resolve Type Exception.({0})", type)) { }
    }

    public class InvokeMethodNotFoundError : InvokeResult
    {
        public InvokeMethodNotFoundError()
            : base(-2, "Method Not Found Exception")
        { }
        public InvokeMethodNotFoundError(string method)
            : base(-2, string.Format("Method Not Found Exception.({0})", method))
        { }
    }

    public class InvokeArgumentException : InvokeResult
    {
        public InvokeArgumentException()
            : base(-3,"Arguments Null Exception") { }
        public InvokeArgumentException(string message)
            :base(-3,message)
        {}
    }

    public class InvokeAuthenticationError : InvokeResult
    {
        public InvokeAuthenticationError() : base(-4, "Authentication Error!") { }
    }

    public class InvokeAuthorizationError : InvokeResult
    {
        public InvokeAuthorizationError() : base(-5, "Authorization error!") { }
    }

    public class InvokeUnknownException:InvokeResult
    {
        public InvokeUnknownException() : this("") { }
        public InvokeUnknownException(string message) : base(-1000, "Unknown Error!\r\n"+message) { }
    }
}
