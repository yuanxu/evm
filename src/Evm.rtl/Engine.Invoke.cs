﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Evm.Rtl
{
    /// <summary>
    /// RTM引擎，同时也是一个DI容器
    /// </summary>
    public sealed partial class Engine
    {

        /// <summary>
        /// 缓存service定义
        /// </summary>
        static Dictionary<string, Schema.Method> m_methodCache = new Dictionary<string, Schema.Method>();


        /// <summary>
        /// 调用方法
        /// </summary>
        /// <param name="type"></param>
        /// <param name="method"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public InvokeResult Invoke(string type, string method, params string[] arguments)
        {
            return Invoke(null, type, method, arguments);
        }

        /// <summary>
        /// 调用方法
        /// </summary>
        /// <param name="method"></param>
        /// <param name="p"></param>
        public InvokeResult Invoke(Func<string, string, bool> authFunc, string type, string method, params string[] arguments)
        {
            //合法性检查
            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(method))
                return new InvokeArgumentException("指定的调用参数不完整！");
            //Type t;
            Type t = null;
            //整理type
            if (type.IndexOf("Service.I") < 0)
            {
                var loc = type.LastIndexOf(".") + 1;
                type = string.Format("{0}Service.I{1}", type.Substring(0, loc), type.Substring(loc));
            }
            try
            {

                t = Engine.Instance.ResolveType(type);
            }
            catch (Exception e)
            {
                log.Error(string.Format("解析类型错误！{0}", e.Message));
                return new InvokeTypeResolveError(type);
            }
            //找到最合适的重载版本 -- 目前版本不支持重载
            var m = t.GetMethod(method);
            if (m == null)
            {
                return new InvokeMethodNotFoundError();
            }
            var parameters = m.GetParameters();
            arguments = arguments == null ? new string[0] : arguments;
            var parameterCount = 0;
            foreach (var p in parameters)
            {
                if (!p.IsOptional)
                    parameterCount++;
            }
            if (arguments.Length < parameterCount)
            {

                return new InvokeArgumentException();
            }

            object[] args = new object[parameters.Length];
            for (int i = 0; i < arguments.Length; i++)
            {
                if (parameters[i].ParameterType.IsPrimitive || parameters[i].ParameterType.Name == "String")
                    args[i] = Convert.ChangeType(arguments[i], parameters[i].ParameterType);
                else
                    args[i] = Newtonsoft.Json.JsonConvert.DeserializeObject(arguments[i], parameters[i].ParameterType);
            }
            InvokeResult result = new InvokeResult();
            //进行权限等检查

            var mKey = string.Format("{0}.{1}", type, method);
            if (!m_methodCache.ContainsKey(mKey))
            {
                var exp = from app in Engine.Instance.LoadedApps
                          from svc in app.Services
                          from mdef in svc.Methods
                          where string.Format("{0}.Service.I{1}", app.Namespace, svc.Name) == type
                          && mdef.ID == method
                          select mdef;
                if (exp.Count() == 1)
                    m_methodCache.Add(type, exp.First());
            }

            if (m_methodCache.ContainsKey(mKey))//需要权限检查
            {
                var mdef = m_methodCache[mKey];
                if (!(string.IsNullOrEmpty(mdef.AuthorizedRoles) && string.IsNullOrEmpty(mdef.AuthorizedUsers)) && authFunc == null)
                    return new InvokeAuthenticationError();
                else if (!authFunc(mdef.AuthorizedRoles, mdef.AuthorizedUsers))
                    return new InvokeAuthorizationError();
            }

            try
            {
                var instance = Engine.Instance.Resolve(t);

                result.Data = m.Invoke(instance, args);
                result.IsSucceed = true;
            }
            catch (Exception e)
            {
                result.ErrorCode = -10;
                result.IsSucceed = false;
                result.ErrorMessage = e.Message + (e.InnerException == null ? "" : " :: " + e.InnerException.Message);
            }
            return result;
        }
    }
}