﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Rtl.Sync
{
    public partial class SyncerBase
    {

        #region Getxxx系列辅助方法

        private IEnumerable<Schema.Entity> GetDropedTable(IList<Schema.Entity> entities, IList<Schema.Entity> lastEntites)
        {
            return lastEntites.Except(entities, new CompareEntityName());
        }

        private IEnumerable<Schema.Index> GetDropedIndex(IList<Schema.Entity> entities, IList<Schema.Entity> lastEntities)
        {
            return (from entity in lastEntities
                    from index in entity.Indexes
                    select index)
                    .Except(
                       (from entity in entities from index in entity.Indexes select index)
                       , new CompareIndex()
                    );
        }

        private IEnumerable<Schema.Property> GetDropedForeignKey(IList<Schema.Entity> entities, IList<Schema.Entity> lastEntities)
        {
            var lastForeignKeys = from entity in lastEntities
                                  from property in entity.Properties
                                  where !string.IsNullOrEmpty(property.ForeignKey)
                                  select property;
            var foreignKeys = from entity in entities
                              from property in entity.Properties
                              where !string.IsNullOrEmpty(property.ForeignKey)
                              select property;
            return lastForeignKeys.Except(foreignKeys, new CompareForeignKey());
        }

        private IEnumerable<Schema.Property> GetDropedProperty(IList<Schema.Entity> entities, IList<Schema.Entity> lastEntities,IEnumerable<Schema.Entity> droppedTable)
        {
            var lastProperties = from entity in lastEntities
                                 from property in entity.Properties
                                 where !property.NoMap && !droppedTable.Contains(entity) //非已删除表中的已删除属性
                                 
                                 select property;
            var properties = from entity in entities
                             from property in entity.Properties
                             where !property.NoMap
                             select property;
            return lastProperties.Except(properties, new ComparePropertyName());
        }

        private IEnumerable<Schema.Property> GetModifiedAndAddedProperty(IList<Schema.Entity> entities, IList<Schema.Entity> lastEntities,IEnumerable<Schema.Entity> addedTable)
        {
            var lastProperties = from entity in lastEntities
                                 from property in entity.Properties
                                 where !property.NoMap
                                 select property;
            var properties = from entity in entities
                             from property in entity.Properties
                             where !addedTable.Contains(property.Entity) && !property.NoMap
                             select property;
            return properties.Except(lastProperties, new CompareProperty());
        }
        private IEnumerable<Schema.Entity> GetAddedTable(IList<Schema.Entity> entities, IList<Schema.Entity> lastEntities)
        {
            return entities.Except(lastEntities, new CompareEntityName());
        }
        /*
        private IEnumerable<Schema.Property> GetAddedProperty(IList<Schema.Entity> entities, IList<Schema.Entity> lastEntities, IEnumerable<Schema.Entity> addedTable)
        {
            var lastProperties = from entity in lastEntities
                                 from property in entity.Properties
                                 where !property.NoMap
                                 select property;
            var properties = from entity in entities
                             from property in entity.Properties
                             where !addedTable.Contains(property.Entity)//不包含新增表的列
                             && !property.NoMap
                             select property;
            return properties.Except(lastProperties, new ComparePropertyName());
        }
        */
        /// <summary>
        /// 获取引用属性的索引
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private IEnumerable<Schema.Index> GetReferedIndex(Schema.Property p)
        {
            var regExpression = string.Format(@"\b{0}\b",p.Column);
            var referedIndex = from index in p.Entity.Indexes
                               where System.Text.RegularExpressions.Regex.IsMatch(index.Columns, regExpression)
                               select index;
            return referedIndex;
        }

        private IEnumerable<Schema.Index> GetAddedIdnex(IList<Schema.Entity> entities, IList<Schema.Entity> lastEntities, IEnumerable<Schema.Entity> addedTable)
        {
            var lastIndexes = from entity in lastEntities
                              from index in entity.Indexes
                              select index;
            var indexes = from entity in entities
                          from index in entity.Indexes
                          where !addedTable.Contains(index.Entity)
                          select index;
            return indexes.Except(lastIndexes, new CompareIndex());
        }
        private IEnumerable<Schema.Property> GetAddedForeignKey(IList<Schema.Entity> entities, IList<Schema.Entity> lastEntities, IEnumerable<Schema.Entity> addedTable)
        {
            var lastForeignKeys = from entity in lastEntities
                                  from property in entity.Properties
                                  where !string.IsNullOrEmpty(property.ForeignKey)
                                  select property;
            var foreignKeys = from entity in entities
                              from property in entity.Properties
                              where !string.IsNullOrEmpty(property.ForeignKey)
                                    && !addedTable.Contains(property.Entity)
                              select property;
            return foreignKeys.Except(lastForeignKeys, new CompareForeignKey());
        }
          private IEnumerable<Schema.Property> GetDropedDefaultConstraint(IList<Schema.Entity> entities, IList<Schema.Entity> lastEntities)
        {
            var defConstraint = from entity in entities
                                from property in entity.Properties
                                where !string.IsNullOrEmpty(property.Default)
                                select property;
            var lastDefConstraint = from entity in lastEntities
                                    from property in entity.Properties
                                    where !string.IsNullOrEmpty(property.Default)
                                    select property;
            return lastDefConstraint.Except(defConstraint, new CompareDefault());
        }
          private IEnumerable<Schema.Property> GetAddedDefaultConstraint(IList<Schema.Entity> entities, IList<Schema.Entity> lastEntities,IEnumerable<Schema.Entity> addedTable)
          {
              var defConstraint = from entity in entities
                                  from property in entity.Properties
                                  where !string.IsNullOrEmpty(property.Default)
                                    && !addedTable.Contains(property.Entity)
                                  select property;
              var lastDefConstraint = from entity in lastEntities
                                      from property in entity.Properties
                                      where !string.IsNullOrEmpty(property.Default)
                                      select property;
              return defConstraint.Except(lastDefConstraint,new CompareDefault());
          }

        #endregion

        #region 比较器
        
        private class CompareEntityName : IEqualityComparer<Schema.Entity>
        {

            public bool Equals(Schema.Entity x, Schema.Entity y)
            {
                return x.Name == y.Name;
            }

            public int GetHashCode(Schema.Entity obj)
            {
                return obj.Name.GetHashCode();
            }
        }

        private class CompareIndex:IEqualityComparer<Schema.Index>
        {

            public bool Equals(Schema.Index x, Schema.Index y)
            {
                return GetIndexHash(x) == GetIndexHash(y);
            }

            private int GetIndexHash(Schema.Index idx)
            {
                return string.Format(idx.Entity.Name+idx.Name+idx.Entity.Name+idx.Columns).GetHashCode();
            }

            public int GetHashCode(Schema.Index obj)
            {
                return GetIndexHash(obj);
            }
        }

        private class CompareForeignKey:IEqualityComparer<Schema.Property>
        {

            public bool Equals(Schema.Property x, Schema.Property y)
            {
                return x.ForeignKey == y.ForeignKey;
            }

            public int GetHashCode(Schema.Property obj)
            {
                return obj.ForeignKey.GetHashCode();
            }
        }
        private class ComparePropertyName:IEqualityComparer<Schema.Property>
        {

            public bool Equals(Schema.Property x, Schema.Property y)
            {
                return x.Name == y.Name;
            }

            public int GetHashCode(Schema.Property obj)
            {
                return obj.Name.GetHashCode();
            }
        }
        class CompareProperty:IEqualityComparer<Schema.Property>
        {

            public bool Equals(Schema.Property x, Schema.Property y)
            {
                return GetHash(x) == GetHash(y);
            }
            int GetHash(Schema.Property p)
            {
                return (p.Entity.Name+p.Name+p.DbType+p.MaxLength+(p.Required?"1":"0")+p.Percision.ToString()+p.Scale.ToString()+p.Default).GetHashCode();
            }
            public int GetHashCode(Schema.Property obj)
            {
                return GetHash(obj);
            }
        }
        class CompareDefault:IEqualityComparer<Schema.Property>
        {

            public bool Equals(Schema.Property x, Schema.Property y)
            {
                return x.Default == y.Default;
            }

            public int GetHashCode(Schema.Property obj)
            {
                return obj.Default.GetHashCode();
            }
        }
        #endregion

    }
}
