﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using Evm.Schema;
using System.Reflection;


namespace Evm.Rtl.Sync
{
    /// <summary>
    /// Schema sync helper 
    /// </summary>
    class SyncHelper
    {
        private static ILog m_log;

        private static IDictionary<string,SyncerBase> syncers;

        /// <summary>
        /// 初始化同步器
        /// </summary>
        /// <param name="coll"></param>
        static  SyncHelper()
        {
            var coll = Evm.Configuration.Environment.Instance.SchemaSyncers;
            syncers = new Dictionary<string, SyncerBase>();
            for (int i = 0; i < coll.Count; i++)
            {
                var type = Type.GetType((coll[i]).Type);
                syncers.Add(coll[i].Name,(SyncerBase)Activator.CreateInstance(type));
            }
        }

        /// <summary>
        /// 日志记录器
        /// </summary>
        private static ILog Log
        {
            get
            {
                if (m_log == null)
                    m_log = LogManager.GetLogger(typeof(SyncHelper));
                return m_log;
            }
        }

        /// <summary>
        /// 同步Schema
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static bool Sync(EvmApp app,Action<String> onMessage = null)
        {
            App lastAppDef = null;
            if (app.LastVersionString != "0.0.0.0" && app.LastVersionString.Length>0)
            {
                lastAppDef =  Registry.GetByVersion(app.Name,app.LastVersionString);
            }

            var exp =syncers.Where(s=>s.Key== app.Options.DbProvider);
            var provider = exp.First().Value;
            return provider.Sync(app, lastAppDef,onMessage);
        }

    }
}
