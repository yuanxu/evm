﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBatisNet.Common.Logging;
using System.Data.Common;
using System.Data;
using System.Text.RegularExpressions;

namespace Evm.Rtl.Sync
{
    /// <summary>
    /// Schema同步基类
    /// 封装基本的数据同步算法及SQL92标准的语句生成。
    /// 特定数据库的同步提供者必须重写GetColumnDefinition方法，必要时重写相应的生成语句(GetxxScript)方法。
    /// </summary>
    public abstract partial class SyncerBase
    {
        #region 日志
        
        private ILog m_log;
        /// <summary>
        /// 日志记录器
        /// </summary>
        protected ILog Log
        {
            get
            {
                if (m_log == null)
                    m_log = LogManager.GetLogger(this.GetType());
                return m_log;
            }
        }

        #endregion
        
        /// <summary>
        /// 数据库连接
        /// </summary>
        protected  DbConnection DbConnection;

        /// <summary>
        /// 获取数据库连接
        /// </summary>
        /// <param name="appName">应用名称</param>
        /// <returns></returns>
        protected virtual DbConnection GetDbConnection(string appName, bool useStandaloneDatabase)
        {
            var cnn = ProviderFactory.CreateConnection();
            var cnnStr = App.Options.DataSource.Substring(0, 1) == "@"
                ? System.Configuration.ConfigurationManager.ConnectionStrings[App.Options.DataSource.Substring(1)].ConnectionString
                : App.Options.DataSource;

            cnn.ConnectionString = cnnStr;
            return cnn;
        }

        /// <summary>
        /// 获取数据库驱动程序工厂
        /// </summary>
        protected abstract DbProviderFactory ProviderFactory { get; }

       
        #region 数据库特性。默认支持外键、索引、默认值、事务；如果不支持需要在具体的同步器中重写相应的属性
       
        /// <summary>
        /// 是否支持事务
        /// </summary>
        public virtual bool SupportTranscation { get { return true; } }

        /// <summary>
        /// 是否支持外键
        /// </summary>
        public virtual bool SupportForeignKey { get { return true; } }

        /// <summary>
        /// 是否支持索引
        /// </summary>
        public virtual bool SupportIndex { get { return true; } }

        /// <summary>
        /// 是否支持默认值
        /// </summary>
        public virtual bool SupportDefaultValue { get { return true; } }

        #endregion

        /// <summary>
        /// 准备语句
        /// </summary>
        /// <param name="firstRun"></param>
        public virtual IList<string> Prepare(bool firstRun,bool useStandaloneDatabase)
        {
            return null;
        }

        /// <summary>
        /// 完成后调用
        /// </summary>
        /// <param name="succeed"></param>
        public virtual IList<string> CleanUp(bool succeed)
        {
            return null;
        }

        /// <summary>
        /// 当前处理的应用定义
        /// </summary>
        protected Schema.App App
        {
            get;
            private set;
        }

        private void FixDbType(IList<Schema.Entity> entities)
        {
            foreach (var p in (from entity in entities 
                               from property in entity.Properties 
                               where !property.NoMap && string.IsNullOrEmpty(property.DbType)
                               select property))
            {
                p.DbType = GetDbType(p.DataType);
            }
        }

        /// <summary>
        /// 同步到数据库
        /// </summary>
        /// <param name="entities">数据库实体集</param>
        ///<param name="lastEntities">上一版本的数据实体集</param>
        /// <returns></returns>
        public bool Sync(Schema.App app,Schema.App lastVersion=null,Action<string> onMessage=null )
        {
            this.App = app;
            bool firstSync = lastVersion == null || !lastVersion.Options.SyncSchemaToDB && app.Options.SyncSchemaToDB;
            //整理数据类型
            FixDbType(app.Entities);
            if (!firstSync)
                FixDbType(lastVersion.Entities);

            //REMARK:头一次编译时需要注释掉这两行
            var sqls = new List<string>();
            var prepareSqls = Prepare(firstSync,app.Options.UseStandaloneDatabase);
            if(prepareSqls!=null)
                sqls.AddRange(prepareSqls);
            
            sqls.AddRange( firstSync ? FirstSync(app.Entities as IList<Schema.Entity>) : SecondSync(app.Entities as IList<Schema.Entity>, lastVersion.Entities)); 
            
            //执行脚本
            
            DbConnection= GetDbConnection(app.Name,app.Options.UseStandaloneDatabase);
            if (DbConnection.State != ConnectionState.Open)
                DbConnection.Open();
            
            foreach (var sql in sqls)
            {
                var log = sql;
                try
                {
                    ExecuteCommand(sql);
                    log += " ... ok";
                    Log.Info(log);
                    if (onMessage != null)
                        onMessage(log);
                }
                catch (Exception e)
                {
                    log += "...Fail." + e.Message;
                    Log.Error(log);
                    if (onMessage != null)
                        onMessage(log);
                    //执行清除语句
                    Log.Info("执行失败！开始执行清除语句....");
                    if (onMessage != null)
                        onMessage("执行失败！开始执行清除语句....");
                    DoCleanUp(false,onMessage);
                    if (DbConnection.State == ConnectionState.Open)
                        DbConnection.Close();
                    return false;
                }   
            }
            DoCleanUp(true,onMessage);
            if (DbConnection.State == ConnectionState.Open)
                DbConnection.Close();
            return true;
        }

        private void DoCleanUp(bool succeed,Action<string> onMessage)
        {
            var cleanUpSqls = CleanUp(succeed);
            if (cleanUpSqls==null)
            {
                return;
            }
            foreach (var csql in cleanUpSqls)
            {
                var log = csql;
                try
                {
                    ExecuteCommand(csql);
                    log += " ... ok";
                    Log.Info(log);
                    if (onMessage != null)
                        onMessage(log);
                    
                }
                catch (Exception err)
                {
                    log += " ... Fail. " + err.Message;
                    Log.Error(log);
                    if (onMessage != null)
                        onMessage(log);
                }
            }
            
        }

        /// <summary>
        /// 首次同步
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        private IList<string> FirstSync(IList<Schema.Entity> entities)
        {
            var result = new List<string>();
            var refScripts = new List<string>();
            
            foreach (var entity in entities)
            {
                if(!entity.IsEnum)
                    result.AddRange(GetNewTableScripts(entities, entity, refScripts));
            }
            //合并外键引用脚本
            result.AddRange(refScripts);
            return result;
        }

        /// <summary>
        /// 非首次执行
        /// </summary>
        /// <param name="entities"></param>
        /// <remarks>
        /// 执行过程：<br />
        /// 1、获取删除的外键(根据外键引用比对)；<br />
        /// 2、获取删除的索引(根据索引名、引用对象名、索引列比对)<br />
        /// 2.1、或是删除的默认值约束(变动即为删除)
        /// 3、获取删除的表(根据表明比对)
        /// 4、获取删除的列(根据列名比对)
        /// 5、获取新增的表
        /// 6、获取修改的列(根据列名、数据类型、长度/精度、默认值、必须性比对)
        /// 7、获取新增的列(不应包含新增表的列)
        /// 8、获取新增的索引(不应包含新增表的索引，比对方法如前,包含新增表索引)
        /// 9、获取新增的外键(不应包含新增表的外键，比对方法如前，包含新增表外键)
        /// 9.1、获取新增的默认值约束(不应包含新增表的默认值)
        /// <b>外键、索引处理策略</b><br />
        /// 首先获取删除的外键及索引，因为索引的比对非常严格，所以如果仅仅是对索引的修改同样会被认为是删除；这么处理的好处是：
        /// 无需生成修改索引的语句，直接在删除变更的索引后，再重新建立即可。
        /// </remarks>
        /// <returns></returns>
        private IList<string> SecondSync(IList<Schema.Entity> entities, IList<Schema.Entity> lastEntities)
        {
            var result = new List<string>();
            //REMARK:所有GetDropedxxx，返回来的均属于lastEntities对象
            //       所有GetModifedxxx,GetAddedxxx，返回来的均属于entites对象

            //1.Droped ForeignKey
            if (SupportForeignKey)
            {
                foreach (var item in GetDropedForeignKey(entities, lastEntities))
                {
                    result.Add(GetDropContraintScript(item.Entity.DbName, GetForeignKeyName(item, lastEntities)));
                }
            }

            var dropedIndex = SupportIndex? GetDropedIndex(entities, lastEntities):null;

            //2.Droped Index
            if (SupportIndex && dropedIndex!=null)
            {
                foreach (var item in dropedIndex)
                {
                    result.Add(GetDropIndexScript(item.Entity.DbName, item.Name));
                }
            }

            //2.1 Droped Default Constraint
            var dropedDefaults= SupportDefaultValue? GetDropedDefaultConstraint(entities, lastEntities):null;

            if (SupportDefaultValue && dropedDefaults!=null)
            {
                foreach (var item in dropedDefaults)
                {
                    result.Add(GetDropContraintScript(item.Entity.DbName, string.Format("DF_{0}_{1}", item.Entity.DbName, item.Column)));
                }
            }

            //3.Droped Table
            var droppedTable =  GetDropedTable(entities, lastEntities);
            foreach (var item in droppedTable)
            {
                result.Add(GetDropTableScript(item.DbName));
            }
            
            //4.Droped Property
            foreach (var item in GetDropedProperty(entities, lastEntities,droppedTable)) //返回来的是上一版本的属性列表（被删除的）
            {
                //需要检查是否有index和default、外键约束；并处理之

                //删除默认值
                if(SupportDefaultValue && !string.IsNullOrEmpty(item.Default)) 
                {
                    result.Add(GetDropContraintScript(item.Entity.DbName,string.Format("DF_{0}_{1}",item.Entity.DbName,item.Column)));
                }
                //删除索引引用
                if (SupportIndex)
                {
                    foreach (var idx in GetReferedIndex(item))
                    {
                        if (dropedIndex==null || !dropedIndex.Contains(idx))//剔除已删除的索引，避免重复删除
                            result.Add(GetDropIndexScript(idx.Entity.DbName, idx.Name));
                    }
                }

                //获取删除列的脚本
                result.Add(GetDropColumnScript(item.Entity.DbName,item.Column));
                
                //修改索引。去掉引用属性的索引，然后重新建立索引
                if (SupportIndex)
                {
                    foreach (var idx in GetReferedIndex(item))
                    {
                        if (dropedIndex!=null && dropedIndex.Contains(idx))//剔除已删除的索引，避免错误重新添加
                            continue;
                        var colsBuilder = new StringBuilder();
                        foreach (var col in idx.Columns.Split(new char[] { ',' }))
                        {
                            if (!Regex.IsMatch(col, @"\b" + item.Column + "\b"))
                            {
                                if (colsBuilder.Length > 0)
                                    colsBuilder.Append(",");
                                colsBuilder.Append(col);
                            }
                        }
                        if (colsBuilder.Length > 0)
                            result.Add(GetCreateIndexScript(idx.Entity.DbName, idx.Name, colsBuilder.ToString(),idx.IsUnique));
                    }
                }
            }

            //5.new table's
            var addedTable = GetAddedTable(entities, lastEntities);
            var refScripts = new List<string>(); //需要后置的脚本
            foreach (var item in addedTable)
            {
                if(!item.IsEnum)
                    result.AddRange(GetNewTableScripts(entities, item, refScripts));
            }

            //6.Modified and added Property
            foreach (var item in GetModifiedAndAddedProperty(entities, lastEntities,addedTable))//返回的是新版本的属性列表(变化的)
            {
                var exp = (from entity in lastEntities
                          from property in entity.Properties
                          where entity.DbName == item.Entity.DbName && property.Name == item.Name
                          select property);
                if (exp.Count() == 0)//added property
                {
                    result.Add(GetAddColumnScript(item.Entity.DbName, GetColumnDefinition(item,false)));
                }
                else
                {
                    //TODO:需要确认如果存在外键或索引是否也需要事先删除之

                    //获取老版本的引用
                    var lastItem = exp.First();
                    //删除默认值
                    //上一版本包含默认值且，此默认值未被删除，则临时删除之
                    if (SupportDefaultValue && !lastItem.NoMap && !string.IsNullOrEmpty(lastItem.Default) && (dropedDefaults == null || !dropedDefaults.Contains(lastItem)))
                    {
                        result.Add(GetDropContraintScript(lastItem.Entity.DbName, string.Format("DF_{0}_{1}", lastItem.Entity.DbName, lastItem.Column)));
                    }

                    //若有索引引用，则临时删除之
                    var tmpDropedIndex = new List<Schema.Index>();
                    if (SupportIndex)
                    {
                        foreach (var idx in GetReferedIndex(lastItem)) //应该剔除已删除的索引
                        {
                            if (dropedIndex == null || !dropedIndex.Contains(idx)) //避免重复删除
                            {
                                result.Add(GetDropIndexScript(idx.Entity.DbName, idx.Name));
                                tmpDropedIndex.Add(idx);
                            }
                        }
                    }

                    //正式的修改列脚本
                    result.Add(GetAlterColumnScript(item.Entity.DbName, item,false));

                    //添加默认值

                    if (SupportDefaultValue && !item.NoMap && !string.IsNullOrEmpty(item.Default))
                    {
                        result.Add(GetAddDefaultScript(item.Entity.DbName, string.Format("DF_{0}_{1}", item.Entity.DbName, item.Column), item.Column,GetDefault(item)));
                    }

                    //重新建立临时删除的索引
                    foreach (var idx in tmpDropedIndex)
                    {
                        result.Add(GetCreateIndexScript(idx.Entity.DbName, idx.Name, idx.Columns,idx.IsUnique));
                    }
                }
            }
            /* 被合并到第六步
            //7.added Property //must not new table's properties
            foreach (var item in GetAddedProperty(entities, lastEntities, addedTable))
            {
                result.Add(GetAddColumnScript(item.Entity.DbName,GetColumnDefinition(item)));
            }
             * */
            //8.added index
            if (SupportIndex)
            {
                foreach (var item in GetAddedIdnex(entities, lastEntities, addedTable))
                {
                    result.Add(GetCreateIndexScript(item.Entity.DbName, item.Name, item.Columns,item.IsUnique));
                }
            }

            //9.added foreignkey
            if (SupportForeignKey)
            {
                foreach (var item in GetAddedForeignKey(entities, lastEntities, addedTable))
                {
                    var refDef = Evm.Utils.RefUtils.GetSimpleRefPart(item.ForeignKey);

                    var refObj = Schema.EntityHelper.GetEntityDbNameByName(entities, refDef.Key);
                    refScripts.Add(GetAddForeignKeyScript(GetForeignKeyName(item, entities)
                        , refObj, refDef.Key + "ID", item.Entity.DbName, item.Column));
                }
            }

            //9.1 added constraint
            if (SupportDefaultValue)
            {
                foreach (var item in GetAddedDefaultConstraint(entities, lastEntities, addedTable))
                {
                    if(!item.NoMap)
                        refScripts.Add(GetAddDefaultScript(item.Entity.DbName, string.Format("DF_{0}_{1}", item.Entity.DbName, item.Column), item.Column, item.Default));
                }
            }
            result.AddRange(refScripts);
            
            return result;
        }

      

        
        private string GetForeignKeyName(Schema.Property p,IList<Schema.Entity> entities)
        {
            var refDef = Utils.RefUtils.GetSimpleRefPart(p.ForeignKey);

            var refObj = Schema.EntityHelper.GetEntityDbNameByName(entities, refDef.Key);
            return string.Format("FK_{0}_REF_{1}", p.Entity.DbName, refObj);
        }
        /// <summary>
        /// 获取创建新表的脚本。
        /// </summary>
        /// <remarks>包含创建表、索引、外键引用等的语句。表和索引在Create Table语句中包含；外键引用需要单独语句且必须所有表创建完毕后执行
        /// </remarks>
        /// <param name="entities">所有表集合--用于外键时检索引用表名</param>
        /// <param name="entity">要生成的表</param>
        /// <param name="refScripts"></param>
        /// <returns></returns>
        private IList<string> GetNewTableScripts(IList<Schema.Entity> entities, Schema.Entity entity, List<string> refScripts)
        {
            List<string> result = new List<string>();
            StringBuilder sb = new StringBuilder();
            foreach (var p in entity.Properties)
            {
                if (p.NoMap)
                    continue;
                if (sb.Length > 0)
                    sb.Append(",");
                sb.AppendLine(GetColumnDefinition(p ));
            }

            var sql = GetCreateTableScript(entity.DbName,sb.ToString(),entity.Name+"ID");

            result.Add(sql);
            //default value
            if (SupportDefaultValue)
            {
                foreach (var p in entity.Properties.Where( tp=>!tp.NoMap && !string.IsNullOrEmpty(tp.Default)))
                {
                    result.Add(GetAddDefaultScript(entity.DbName, string.Format("DF_{0}_{1}", p.Entity.DbName, p.Column),p.Column,GetDefault(p)));
                }
            }
            if (SupportIndex)
            {
                //index
                foreach (var idx in entity.Indexes)
                {
                    result.Add(GetCreateIndexScript(entity.DbName, idx.Name, idx.Columns,idx.IsUnique));
                }
            }

            if (SupportForeignKey)
            {
                //:foreign key
                foreach (var item in entity.Properties.Where(p => !string.IsNullOrEmpty(p.ForeignKey)))
                {
                    var refDef = Utils.RefUtils.GetSimpleRefPart(item.ForeignKey);

                    var refObj = Schema.EntityHelper.GetEntityDbNameByName(entities, refDef.Key);
                    refScripts.Add(GetAddForeignKeyScript(string.Format("FK_{0}_REF_{1}", item.Entity.DbName, refObj)
                        , refObj, refDef.Key + "ID", item.Entity.DbName, item.Column));
                }
            }
            return result;
        }

       
        /// <summary>
        /// 获取列定义。
        /// 如果未提供DataType属性，提供者应该根据Type属性自动映射相应的DataType。
        /// </summary>
        /// <param name="p">属性</param>
        /// <param name="includeDefaultValue">是否包含默认值</param>
        /// <returns></returns>
        protected abstract string GetColumnDefinition(Schema.Property p,bool newDb=true);

        /// <summary>
        /// 根据type获取对应的dbType
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected abstract string GetDbType(string type);

        /// <summary>
        /// 获取列的默认值定义
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        protected virtual  string GetDefault(Schema.Property p)
        {
            return p.Default;
        }
        #region SQL 脚本元素，SQL 92标准；可被具体数据库提供者重写

        /// <summary>
        /// 获取Create Table语句脚本
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="colDefinitions"></param>
        /// <param name="pkeyName"></param>
        /// <returns></returns>
        protected virtual string GetCreateTableScript(string tableName,string colDefinitions,string pkeyName="")
        {
            return string.Format("CREATE TABLE {0}({1}{2})", tableName, colDefinitions, 
                string.IsNullOrEmpty(pkeyName)?"":string.Format(", CONSTRAINT PK_{0} PRIMARY KEY ({0})",pkeyName));
        }

        /// <summary>
        /// 获取Create Index于是脚本
        /// </summary>
        /// <param name="idxName"></param>
        /// <param name="tableName"></param>
        /// <param name="cols"></param>
        /// <returns></returns>
        protected virtual string GetCreateIndexScript(string tableName, string idxName, string cols, bool isUnique)
        {
            return string.Format("CREATE {3} INDEX {0} ON {1}({2})", idxName, tableName, cols, isUnique ? "UNIQUE" : "");
        }

        /// <summary>
        /// 获取重建外键脚本
        /// </summary>
        /// <param name="fkName">外键名称</param>
        /// <param name="parentTableName">主表表名</param>
        /// <param name="parentTableColumn">主表列名</param>
        /// <param name="childTableName">子表表名</param>
        /// <param name="childColumn">子表列名</param>
        /// <returns></returns>
        protected virtual string GetAddForeignKeyScript(string fkName, string parentTableName, string parentTableColumn, string childTableName, string childColumn)
        {
            return string.Format("ALTER TABLE {1} ADD CONSTRAINT {0} FOREIGN KEY ({2}) REFERENCES  {3} ({4})"
                , fkName, childTableName, childColumn, parentTableName, parentTableColumn);
        }
        protected virtual string GetDropContraintScript(string tableName, string foreignKeyname)
        {
            return string.Format("ALTER TABLE {0} DROP CONSTRAINT {1} ", tableName, foreignKeyname);
        }
        protected virtual string GetAddColumnScript(string tableName, string columnDefinition)
        {
            return string.Format("ALTER TABLE {0} ADD {1} ", tableName, columnDefinition);
        }

        protected virtual string GetAddDefaultScript(string tableName, string constraintName, string column, string defaultVal)
        {
            return string.Format("ALTER TABLE {0} ADD CONSTRAINT {1} DEFAULT {3} FOR {2}", tableName, constraintName, column, defaultVal);
        }

        protected virtual string GetAlterColumnScript(string tableName, Schema.Property p,bool newDb=true)
        {

            return string.Format("ALTER TABLE {0} ALTER COLUMN {1}",tableName, GetColumnDefinition(p,newDb));
        }

        protected virtual string GetDropColumnScript(string tableName, string column)
        {
            return string.Format("ALTER TABLE {0} DROP COLUMN {1}", tableName, column);
        }

        protected virtual string GetDropIndexScript(string tableName, string indexName)
        {
            return string.Format("DROP INDEX {0}.{1}", tableName, indexName);
        }

        protected virtual string GetDropTableScript(string tableName)
        {
            return string.Format("DROP TABLE {0}", tableName);
        }

        #endregion

        /// <summary>
        ///执行SQL命令
        /// </summary>
        /// <param name="sql">sql命令</param>
        /// <returns></returns>
        public virtual int ExecuteCommand(string sql)
        {
            var cmd = DbConnection.CreateCommand();
            cmd.CommandText = sql;
            return cmd.ExecuteNonQuery();
        }
    }
}
