﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Common;
using Evm.Schema;

namespace Evm.Rtl.Sync.Provider
{
    /// <summary>
    /// Microsoft SQL Server 2kx Syncer
    /// 
    /// <remarks>
    /// 特性：为每个App建立独立的数据库；<br />
    /// 支持主键、外键、索引、默认值约束，并自动为外键建立索引；<br />
    /// </remarks>
    /// 
    /// </summary>
    public class MsSqlSyncer : SyncerBase
    {
        

        protected override DbProviderFactory ProviderFactory
        {
            get { return SqlClientFactory.Instance; }
        }
        protected override DbConnection GetDbConnection(string appName, bool useStandaloneDatabase)
        {
            var cnn = base.GetDbConnection(appName, useStandaloneDatabase);
            if (!useStandaloneDatabase)
                return cnn;
            var loc = cnn.ConnectionString.IndexOf("Initial Catalog");
            if (loc > 0)
            {
                var substr =cnn.ConnectionString.Substring(loc);
                cnn.ConnectionString = cnn.ConnectionString.Substring(0, loc)
                    + substr.Substring(substr.IndexOf(";")+1);
            }
            return cnn;
        }

        public override IList<string> Prepare(bool firstRun, bool useStandaloneDatabase)
        {
            List<string> result = new List<string>();
            if (firstRun && useStandaloneDatabase)
            {
                result.Add(string.Format("CREATE DATABASE {0}",App.Name));
                
            }
            result.Add(string.Format("USE {0}", App.Name));
            result.Add("BEGIN TRANSACTION ");
            return result;
        }
        public override IList<string> CleanUp(bool succeed)
        {
            var result = new List<string>();
            if (succeed)
                result.Add("COMMIT TRANSACTION ");
            else
                result.Add("ROLLBACK TRANSACTION ");
            return result;
        }
        /// <summary>
        /// 获取列定义
        /// </summary>
        /// <param name="p"></param>
        /// <param name="includeDefaultVal">是否包含默认值约束</param>
        /// <returns></returns>
        protected  override string GetColumnDefinition(Schema.Property p,bool newDb =true)
        {
          
            if (p.NoMap)
                return "";
            var dataType = p.DbType.ToLower();
            if (dataType.IndexOf("char") >= 0)
                dataType = string.Format("{0}({1})", dataType, p.MaxLength);


            if (dataType == "guid")
            {
                dataType = "uniqueidentifier";

            }

            var nullAble = p.Required ? "NOT NULL" : "NULL";
            var identity = "";
            if (newDb && (dataType == "int" || dataType == "long") && p.Column == p.Entity.Name + "ID")
                identity = "IDENTITY";

            return string.Format("{0} {1} {2} {3}", p.Column, dataType, nullAble, identity);
        }

        /// <summary>
        /// 根据IBatis文档中3.7. Supported database types for Parameter Maps and Result Maps实现。
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected override string GetDbType(string type)
        {
            
            switch (type.ToLower())
            {
                case "short":
                    return "SmallInt";
                case "int":
                    return "Int";
                case "long":
                    return "BigInt";
                case "ushort":
                    return "Int";
                case "uint":
                    return "Decimal";
                case "ulong":
                    return "Decimal";    
                case "byte":
                    return "TinyInt";  
                case "datetime":
                    return "DateTime";
                case "decimal":
                    return "Money";
                case "float":
                    return "Real";
                case "double":
                    return "Float";
                case "string":
                    return "nvarchar";
                case "bool":
                    return "bit";
                case "guid":
                    return "uniqueidentifier";
                default: //not support char、sbyte
                    throw new EvmCompileException(string.Format("不支持的数据类型{0}！",type));
            }
        }
        /// <summary>
        /// 获取默认值定义
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        protected override  string GetDefault(Schema.Property p)
        {
            var defaultVal = "";
            if (!string.IsNullOrEmpty(p.Default))
            {
                switch (p.DataType.ToLower())
                {
                    case "datetime":
                        defaultVal = "getdate()";
                        break;
                    case "guid":
                        defaultVal = "NEWID()";
                        break;
                    default:
                        defaultVal = p.Default;
                        break;
                }
            }
            return defaultVal;
        }
       
        public override bool SupportTranscation
        {
            get
            {
                return true;
            }
            
        }

      
    }
}
