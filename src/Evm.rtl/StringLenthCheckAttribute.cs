﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace Evm.Rtl
{
    /// <summary>
    /// 字符串数值检查
    /// </summary>
    public class StringLenthCheckAttribute:ValidationAttribute
    {
        private int m_min, m_max;

        public StringLenthCheckAttribute(int min, int max, string errorMessage)
            :base(errorMessage)
        {
            m_min = min;
            m_max = max;
        }

        public override bool IsValid(object value)
        {
            int len = value==null?0: value.ToString().Length;
            return len >= m_min && len <= m_max;
        }
    }
}
