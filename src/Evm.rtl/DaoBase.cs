﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using IBatisNet.DataMapper;
using System.Collections;

namespace Evm.Rtl
{
    /// <summary>
    /// DAO基类
    /// </summary>
    public abstract class DaoBase
    {
        private ILog m_log;
        /// <summary>
        /// 日志记录器
        /// </summary>
        protected ILog Log
        {
            get
            {
                if (m_log == null)
                    m_log = LogManager.GetLogger(this.GetType());
                return m_log;
            }
        }
        protected Hashtable GetSortHashtable(Hashtable ht,string sortColumn, string sortDirection)
        { 
            if (!string.IsNullOrEmpty(sortColumn))
            {
                {
                    ht.Add("sortColumn", sortColumn);
                    ht.Add("sortDirection", sortDirection);
                }
            }
            return ht;
        }
        protected Hashtable GetSortHashtable(string sortColumn,string sortDirection)
        {
            return GetSortHashtable(new Hashtable(), sortColumn, sortDirection);
        }

        /// <summary>
        /// 获取当前服务的映射
        /// </summary>
        protected abstract SqlMapper MyMapper { get; }
    }
}
