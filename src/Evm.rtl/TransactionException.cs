﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Rtl
{
    /// <summary>
    /// 事务异常
    /// </summary>
    public class TransactionException :EvmRtException
    {
        public TransactionException(string message) : base(message) { }
        public TransactionException(string message, Exception inner) : base(message, inner) { }
    }
}
