﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Evm.Configuration;
using ICSharpCode.SharpZipLib.Zip;
using log4net;
using Newtonsoft.Json;

namespace Evm.Rtl
{
    /// <summary>
    /// RTM引擎，同时也是一个DI容器
    /// </summary>
    public  sealed partial class Engine
    {
        private IWindsorContainer container; 
        private Engine()
        {
            container = new WindsorContainer();
            //AppDomain.CurrentDomain.AppendPrivatePath(Evm.Configuration.Environment.Instance.AppFolder);
           // Evm.Configuration.Environment.Instance.AppFolder = Evm.Configuration.Environment.Instance.BinDir;
        }

        #region Log
        private ILog log = LogManager.GetLogger(typeof(Engine));
        private ILog Log
        {
            get { return log; }
        }
        #endregion

        #region 单件

        private static Engine m_Instance;
        public static Engine Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    lock (typeof(Engine))
                        if (m_Instance == null)
                        {
                            m_Instance = new Engine();
                           
                        }
                }
                return m_Instance;
            }
        }

        #endregion

        #region 应用上载
        public event EventHandler<EngineEventArgs> OnMessage;

        private void PublishMessage(object sender,string message)
        {
            if (OnMessage != null)
                OnMessage(sender, new EngineEventArgs(message));
        }

        /// <summary>
        /// 上载App,需要编译、存储到核心配置库
        /// </summary>
        /// <param name="stream"></param>
        public void UploadApp(Stream stream)
        {
            //解压
            //上传
            var fastZip = new FastZip();

            var targetDir = string.Format("{0}\\Temp\\{1}",Evm.Configuration.Environment.Instance.AppFolder,DateTime.Now.Millisecond);
            fastZip.ExtractZip(stream, targetDir, FastZip.Overwrite.Always, null, "", "", false,true);
            UploadApp(string.Format("{0}\\App.xml", targetDir));
        }
        
        /// <summary>
        /// 上载App，需要编译、存储到核心配置库
        /// </summary>
        /// <param name="appFileName">已上载的应用定义文件名</param>
        public void UploadApp(string appFileName)
        {
            var app = AppBuilder.Instance.Parse(appFileName).ToEvmApp();
            var lastVer = Registry.GetLast(app.Name);
            if (lastVer == null || lastVer.Version < app.Version)
            {
                var compilers = new List<CompilerBase2>();
                foreach (var item in Evm.Configuration.Environment.Instance.RtlCompilers)
                {
                    var type = Type.GetType(((CompilerElement)item).Type);
                    var compiler = (CompilerBase2)Activator.CreateInstance(type);

                    compiler.Setup(app, Evm.Configuration.Environment.Instance.BinDir, string.Format("{0}\\{1}\\Temp", Evm.Configuration.Environment.Instance.AppFolder, app.Name));
                    compilers.Add(compiler);
                }
                PublishMessage(Instance, "开始编译....");
                //编译
                //TODO:需要获取编译错误
                AppBuilder.Instance.Compile(appFileName, compilers);

                //保存历史版本
                CopyToAppFolder(appFileName, app);


                app.ExecutedInstall = false;
                app.SyncedSchema = false;
                app.LastVersionString =lastVer==null?"0.0.0.0": lastVer.VersionString;
                //成功后保存
                Registry.Save(app);
            }
            else
            {
                app = lastVer;//app == lastVer,but lastVer contain additional information
                PublishMessage(Instance, "应用是最新的，无需重新编译.");
            }
            //同步         
            SyncSchema(app);

        }

        /// <summary>
        /// 同步Schema
        /// </summary>
        /// <param name="app"></param>
        private static void SyncSchema(EvmApp app)
        {
            if (!app.Options.SyncSchemaToDB || app.SyncedSchema)
                return;
            
            Instance.PublishMessage(Instance, "开始同步Schema...");
            if (Sync.SyncHelper.Sync(app, msg => Instance.PublishMessage(Instance, msg)))
            {
                app.ExecutedInstall = false;
                app.SyncedSchema = true;
                //成功后保存
                Registry.Save(app);
            }
        }

       
        private void CopyToAppFolder(string appFileName,Schema.App app)
        {
            var targetDir = string.Format("{0}\\{1}\\{2}",Evm.Configuration.Environment.Instance.AppFolder
                ,app.Name,app.VersionString);
            
            if (!Directory.Exists(string.Format("{0}\\{1}", Evm.Configuration.Environment.Instance.AppFolder, app.Name)))
                Directory.CreateDirectory(string.Format("{0}\\{1}", Evm.Configuration.Environment.Instance.AppFolder, app.Name));
            
            if (!Directory.Exists(targetDir))
                Directory.CreateDirectory(targetDir);

            FileInfo info = new FileInfo(appFileName);
            File.Copy(appFileName, string.Format("{0}\\{1}", targetDir, "app.xml"),true);

            foreach (var item in app.EntityDefinitions)
            {
                //处理包含子目录情况
                if (item.IndexOf("\\") > 0)
                {
                    var subDir = string.Format("{0}\\{1}", targetDir, item.Substring(0, item.IndexOf("\\")));
                    if (!Directory.Exists(subDir))
                        Directory.CreateDirectory(subDir);
                }
                
                File.Copy(string.Format("{0}\\{1}", info.DirectoryName, item), string.Format("{0}\\{1}", targetDir, item),true);
            }
            foreach (var item in app.ServiceDefinitions)
            {
                //处理包含子目录情况
                if (item.IndexOf("\\") > 0)
                {
                    var subDir = string.Format("{0}\\{1}", targetDir, item.Substring(0, item.IndexOf("\\")));
                    if (!Directory.Exists(subDir))
                        Directory.CreateDirectory(subDir);
                }
                File.Copy(string.Format("{0}\\{1}", info.DirectoryName, item), string.Format("{0}\\{1}", targetDir, item),true);
            }
        }
        
        #endregion

        #region 应用装载
        
        /// <summary>
        /// 已装载的AppDef
        /// </summary>
        private List<EvmApp> loadedApp = new List<EvmApp>();

        /// <summary>
        /// 装载App。此App已经过编译，并存于系统数据库中
        /// </summary>
        /// <param name="appName"></param>
        public void Load(string appName,bool syncSchema=true)
        {
            //如果已经加载过
            if (loadedApp.Where(a => a.Name == appName).Count() > 0)
                return;
            var app = Registry.GetByName(appName);
            if (app == null)
            {
                throw new ArgumentException(string.Format("应用{0}不存在！",appName));                
            }
            //var appContainer = new WindsorContainer();
            loadedApp.Add(app);
            //注册类
            
            container.Register(AllTypes.FromAssemblyInDirectory(new Castle.MicroKernel.Registration.AssemblyFilter(Evm.Configuration.Environment.Instance.BinDir))
                .Where(t => t.Namespace.EndsWith(".Dao") || t.Namespace.EndsWith(".Service") || t.Namespace.EndsWith(".Impl")
                ).WithService.DefaultInterfaces());
            if (syncSchema && app.Options.SyncSchemaToDB && !app.SyncedSchema)
            {
                var parsedApp = AppBuilder.Instance.Parse(string.Format(@"{0}\{1}\{2}\app.xml",Evm.Configuration.Environment.Instance.AppFolder,app.Name,app.VersionString)).ToEvmApp();
                parsedApp.LastVersionString = app.LastVersionString;
                
                SyncSchema(parsedApp);
            }
            //执行安装
            if (!app.ExecutedInstall && !string.IsNullOrEmpty(app.Options.InstallMethod))
            {
                Install(app);
            }
        }

        /// <summary>
        /// 安装应用
        /// </summary>
        /// <param name="app"></param>
        private void Install(EvmApp app)
        {
            if (app.ExecutedInstall || string.IsNullOrEmpty(app.Options.InstallMethod))
                return;

            Log.InfoFormat("开始安装{0}", app.Name);
            try
            {
                var kv = Utils.RefUtils.GetSimpleRefPart(app.Options.InstallMethod);
                Invoke(string.Format("{0}.Service.I{1}", app.Namespace, kv.Key), kv.Value
                    , new string[] { JsonConvert.SerializeObject(app.Version), JsonConvert.SerializeObject(Version.Parse(app.LastVersionString)) }
                    );
                app.ExecutedInstall = true;
                Registry.Save(app);
                log.InfoFormat("{0}安装完成！", app.Name);
            }
            catch (Exception e)
            {
                log.ErrorFormat("{0}安装失败！\r\n{1}", app.Name, e.Message);
                throw;
            }
        }

        /// <summary>
        /// 装载所有应用
        /// </summary>
        public void LoadAll()
        {
            foreach (var app in Registry.GetList())
            {
                Load(app.Name);
            }
        }

       
        private IEnumerable<Type> GetRegisterableTypes(Assembly assembly)
        {
            var result = assembly.GetExportedTypes().Where
                (
                    t => t.IsClass &&
                                    t.IsAbstract == false &&
                                    t.IsGenericTypeDefinition == false &&
                                    t!=typeof(EntityBase)
                );
            return result;
        }

        /// <summary>
        /// 卸载App
        /// </summary>
        /// <param name="appName"></param>
        public void Unload(string appName)
        {
            var appContainer = container.GetChildContainer(appName);
            
            container.RemoveChildContainer(appContainer);
            appContainer.Dispose();
        }

        /// <summary>
        /// 已加载的App
        /// </summary>
        public IList<EvmApp> LoadedApps
        {
            get { return loadedApp; }
        }
        #endregion


        public void Setup(string appDir,string binDir)
        {
            Configuration.Environment.Instance.AppFolder = appDir;
            Configuration.Environment.Instance.BinDir = binDir;
            
        }

        
    }
    public class EngineEventArgs :EventArgs
    {
        public EngineEventArgs() : this("") { }
        public EngineEventArgs(string message) { Message = message; }
        /// <summary>
        /// 消息
        /// </summary>
        public string Message { get; set; }
    }
}
