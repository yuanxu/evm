﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;
using System.Xml;
using System.Reflection;
using System.IO;


namespace Evm.Rtl
{
    public class MyMapper
    {
        
        private Dictionary<string, SqlMapper> m_Mappers;
        private static MyMapper m_Instance;
        private MyMapper()
        {
            m_Mappers = new Dictionary<string, SqlMapper>();
        }
        /// <summary>
        /// Get the instance of the SqlMapper defined by the SqlMap.Config file.
        /// </summary>
        /// <returns>A SqlMapper initalized via the SqlMap.Config file.</returns>
        public static SqlMapper Instance(string appName)
        {
            
            if (m_Instance == null)
            {
                lock (typeof(SqlMapper))
                {
                    if (m_Instance == null)
                        m_Instance = new MyMapper();
                }
            }
            if (m_Instance.m_Mappers.ContainsKey(appName))
                return m_Instance.m_Mappers[appName];
            else
            {
                lock (m_Instance)
                {
                    SqlMapper _Mapper = m_Instance.InitMapper(appName);
                    m_Instance.m_Mappers.Add(appName, _Mapper);
                    return _Mapper;
                }
            }
        }

        /// <summary>
        /// Init the 'default' SqlMapper defined by the SqlMap.Config file.
        /// </summary>
        private SqlMapper InitMapper(string appName)
        {
            DomSqlMapBuilder builder = new DomSqlMapBuilder();
            Assembly ass = Assembly.LoadFrom(string.Format("{0}\\{1}.ServiceImpl.dll", Evm.Configuration.Environment.Instance.BinDir, appName));
            XmlDocument sqlMapConfig = new XmlDocument();
            var stream = ass.GetManifestResourceStream("SqlMap.config");
            if (stream == null)
                stream = ass.GetManifestResourceStream(string.Format("{0}.ServiceImpl.SqlMap.config", appName));
            using (StreamReader reader = new StreamReader(stream))
            {
                sqlMapConfig.Load(reader.BaseStream);   
            }
             
            SqlMapper _Mapper = (SqlMapper)builder.Configure(sqlMapConfig);
            _Mapper.SessionStore = new IBatisNet.DataMapper.SessionStore.HybridWebThreadSessionStore(_Mapper.Id);
            //修改应用的数据库连接
            var cnnStr = Registry.GetByName(appName).Options.DataSource;
            if (cnnStr.Substring(0,1)=="@") //引用.Config文件的
            {
                cnnStr = System.Configuration.ConfigurationManager.ConnectionStrings[cnnStr.Substring(1)].ConnectionString;
            }
            _Mapper.DataSource.ConnectionString = cnnStr;
            return _Mapper;
        }

    }
}
