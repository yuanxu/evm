﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Rtl
{
    /// <summary>
    /// Model验证失败异常
    /// </summary>
    public class InvalidModelException : Rtl.EvmRtException
    {
        public InvalidModelException()
            :base("模型数据验证失败！")
        { }
    }
}
