﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace Evm.Rtl
{

    /// <summary>
    /// 分页查询结果
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PagedList<T>
    {
       
        private int m_PageSize=20, m_Page, m_PageCount;
        private int m_RecordCount;
        
        public PagedList(int pageIndex)
        {
            m_Page = pageIndex;
        }
        public PagedList(int pageIndex,IList<T> data)
        {
            m_Page = pageIndex;
            m_Data = data;
        }
        public PagedList(int pageIndex,IList<T> data,int recordCount)
        {
            m_Page = pageIndex;
            m_Data = data;
            RecordCount = recordCount;
        }

        /// <summary>
        /// 记录总数
        /// </summary>
        public int RecordCount
        {
            get { return m_RecordCount; }
            set 
            { 
                m_RecordCount = value;
                m_PageCount = value % m_PageSize == 0 ? value / m_PageSize : value / m_PageSize + 1;
            }
        }
        private IList<T> m_Data;

        /// <summary>
        /// 当前页数据
        /// </summary>
        public IList<T> Data
        {
            get { return m_Data; }
            set { m_Data = value; }
        }
        /// <summary>
        /// 总页数
        /// </summary>
        public int PageCount
        {
            get { return m_PageCount; }
            set { m_PageCount = value; }
        }

        /// <summary>
        /// 当前页
        /// </summary>
        public int Page
        {
            get { return m_Page; }
            set { m_Page = value; }
        }

        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize
        {
            get { return m_PageSize; }
            set { m_PageSize = value; }
        }
        public override string ToString()
        {
            return  string.Format("共有{0}条数据，第{1}页，每页{2}条数据", m_RecordCount, m_Page, m_PageSize);
        }

       
    }
}
