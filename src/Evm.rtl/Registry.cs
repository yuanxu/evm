﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace Evm.Rtl
{
    /// <summary>
    /// 应用程序注册表
    /// </summary>
    internal class Registry
    {
        /// <summary>
        /// 数据库名称
        /// </summary>
        private const string DbName = "Registry.xml";
        private static XElement doc;
        private static IList<EvmApp> apps;

        static Registry()
        {
            var fileName = GetDbFileName();
            if (!File.Exists(fileName))
            {
                doc = XElement.Parse(@"<?xml version='1.0' encoding='utf-8'?><registry></registry>");
            }
            else
            {
                doc = XElement.Load(GetDbFileName());
            }
            apps = new List<EvmApp>();
            foreach (var item in doc.Elements())
            {
                apps.Add(new EvmApp()
                    {
                        Name = item.Attribute("name").Value,
                        Author = item.Attribute("author").Value,
                        Description = item.Attribute("description").Value,
                        Options = new Schema.App.AppOptions()
                        {
                            DataSource = item.Attribute("dataSource").Value,
                            DbProvider = item.Attribute("dbProvider").Value,
                            InstallMethod = item.Attribute("installMethod") != null ? item.Attribute("installMethod").Value : "",
                            UninstallMethod = item.Attribute("uninstallMethod") != null ? item.Attribute("uninstallMethod").Value : "",
                            UseStandaloneDatabase = item.Attribute("useStandaloneDatabase")!=null?Convert.ToBoolean(item.Attribute("useStandaloneDatabase").Value):true
                        },
                        Namespace = item.Attribute("namespace").Value,
                        VersionString = item.Attribute("version").Value,
                        ExecutedInstall = item.Attribute("executedInstall") != null ? Convert.ToBoolean(item.Attribute("executedInstall").Value) : true,
                        LastVersionString = item.Attribute("lastVersion") != null ? item.Attribute("lastVersion").Value : "",
                        SyncedSchema = item.Attribute("syncedSchema") != null ? Convert.ToBoolean(item.Attribute("syncedSchema").Value) : true

                    });
            }
        }

        private static string GetDbFileName()
        {
            var dirs = new[] { AppDomain.CurrentDomain.SetupInformation.ApplicationBase, AppDomain.CurrentDomain.SetupInformation.PrivateBinPath };
            foreach (var dir in dirs)
            {
                var file = string.Format("{0}\\{1}", dir, DbName);
                if (System.IO.File.Exists(file))
                    return file;
            }
            return "";
        }

        /// <summary>
        /// 获取所有已经编译/上载的应用
        /// </summary>
        /// <returns></returns>
        public static IList<EvmApp> GetList()
        {
            return apps;
        }

        /// <summary>
        /// 根据应用名称获取应用
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        public static EvmApp GetByName(string appName)
        {
            var exp = apps.Where(a=>a.Name==appName);
            if (exp.Count() > 0)
                return exp.First();
            else
                return null;
        }

        public static EvmApp GetLast(string appName)
        {
            var ap = GetByName(appName);
            if (ap == null)
                return null;
            else
                return Combin( AppBuilder.Instance.Parse(string.Format("{0}\\{1}\\{2}\\app.xml", Evm.Configuration.Environment.Instance.AppFolder, ap.Name, ap.VersionString)),ap);
        }

        public static EvmApp GetByVersion(string appName,string ver)
        {
            var ap = GetByName(appName);
            if (ap == null)
                return null;
            else
                return Combin( AppBuilder.Instance.Parse(string.Format("{0}\\{1}\\{2}\\app.xml", Evm.Configuration.Environment.Instance.AppFolder, ap.Name, ver)),ap);
        }
        private static EvmApp Combin(Evm.Schema.App a,EvmApp app)
        {
            var r = a.ToEvmApp();
            r.LastVersionString = app.LastVersionString;
            r.ExecutedInstall = app.ExecutedInstall;
            r.SyncedSchema = app.SyncedSchema;
            return r;
        }
        /// <summary>
        /// 指定的命名空间是否可用
        /// </summary>
        /// <param name="nameSpace"></param>
        /// <returns></returns>
        public static bool NamespaceExists(string nameSpace)
        {
            return apps.Where(a => a.Namespace == nameSpace).Count() > 0;
        }

        /// <summary>
        /// 移除应用
        /// </summary>
        /// <param name="appName"></param>
        public static void Remove(string appName)
        {
            var exp = doc.Elements().Where(item => item.Attribute("name").Value == appName);
            if (exp.Count() == 0)
                throw new NullReferenceException("指定的应用不存在");
            exp.Remove();
            apps.Remove( apps.Where(a => a.Name == appName).First());
            SaveToFile();
        }

        /// <summary>
        /// 保存应用
        /// </summary>
        /// <param name="app"></param>
        public static void Save(EvmApp app)
        {
            var exp = doc.Elements().Where(item => item.Attribute("name").Value == app.Name);
            if (exp.Count() == 0)
            {
                apps.Add(app);
                doc.Add(new XElement("app"
                        , new XAttribute("name", app.Name)
                        , new XAttribute("version", app.VersionString)
                        , new XAttribute("author", app.Author)
                        , new XAttribute("namespace", app.Namespace)
                        , new XAttribute("description", app.Description)
                        , new XAttribute("dataSource", app.Options.DataSource)
                        , new XAttribute("dbProvider", app.Options.DbProvider)
                        , new XAttribute("installMethod", app.Options.InstallMethod)
                        , new XAttribute("uninstallMethod", app.Options.UninstallMethod)
                        , new XAttribute("executedInstall", app.ExecutedInstall.ToString())
                        , new XAttribute("lastVersion",app.LastVersionString)
                        , new XAttribute("syncedSchema",app.SyncedSchema.ToString())
                        , new XAttribute("useStandaloneDatabase",app.Options.UseStandaloneDatabase.ToString())
                    ));
            }
            else
            {
                var xe = exp.First();
                //新版本
                if (xe.Attribute("version").Value != app.VersionString)
                {
                    app.LastVersionString = xe.Attribute("version").Value;
                    xe.Attribute("version").Value = app.VersionString;
                    xe.Attribute("author").Value = app.Author;
                    xe.Attribute("namespace").Value = app.Namespace;
                    xe.Attribute("description").Value = app.Description;
                    xe.Attribute("dataSource").Value = app.Options.DataSource;
                    xe.Attribute("dbProvider").Value = app.Options.DbProvider;
                    if (xe.Attribute("executedInstall") == null)
                    {
                        xe.Add(new XAttribute("executedInstall", ""));
                        xe.Add(new XAttribute("installMethod", ""));
                        xe.Add(new XAttribute("uninstallMethod", ""));
                        xe.Add(new XAttribute("lastVersion", ""));
                        xe.Add(new XAttribute("useStandaloneDatabase", ""));
                    }
                    xe.Attribute("executedInstall").Value = app.ExecutedInstall.ToString();
                    xe.Attribute("installMethod").Value = app.Options.InstallMethod;
                    xe.Attribute("uninstallMethod").Value = app.Options.UninstallMethod;

                    xe.Attribute("lastVersion").Value = app.LastVersionString;
                    xe.Attribute("syncedSchema").Value = app.SyncedSchema.ToString();
                    xe.Attribute("useStandaloneDatabase").Value = app.Options.UseStandaloneDatabase.ToString();
                }
                else //老版本的部分修改
                {
                    xe.Attribute("executedInstall").Value = app.ExecutedInstall.ToString();
                    xe.Attribute("installMethod").Value = app.Options.InstallMethod;
                    xe.Attribute("uninstallMethod").Value = app.Options.UninstallMethod;
                    xe.Attribute("syncedSchema").Value = app.SyncedSchema.ToString();
                }
                var ai = apps.Where(a => a.Name == app.Name).First();
                ai.VersionString = app.VersionString;
                ai.Author = app.Author;
                ai.Namespace = app.Namespace;
                ai.Description = app.Description;
                ai.Options.DataSource = app.Options.DataSource;
                ai.Options.DbProvider = app.Options.DbProvider;
                ai.Options.InstallMethod = app.Options.InstallMethod;
                ai.Options.UninstallMethod = app.Options.UninstallMethod;
                ai.Options.UseStandaloneDatabase=app.Options.UseStandaloneDatabase;
                ai.ExecutedInstall = app.ExecutedInstall;
                ai.LastVersionString = app.LastVersionString;
                ai.SyncedSchema = app.SyncedSchema;
            }
            SaveToFile();
        }
        private static void SaveToFile()
        {
            doc.Save(string.Format("{0}\\{1}",Evm.Configuration.Environment.Instance.BinDir, DbName));
        }
    }
}
