﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Rtl
{
    /// <summary>
    /// 运行校验错误
    /// </summary>
    public class RuleViolation
    {
        public string ErrorMessage { get; set; }
        public string PropertyName { get; set; }

        public RuleViolation(string propertyName, string errorMessage)
        {
            PropertyName = propertyName;
            ErrorMessage = errorMessage;
        }
    }
}
