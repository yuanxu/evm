﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Rtl
{
    /// <summary>
    /// Evm运行时异常
    /// </summary>
    public class EvmRtException:ApplicationException
    {
        public int ErrorNumber { get; set; }
        public EvmRtException(int errorNumber){ErrorNumber=errorNumber;}
        public EvmRtException(int errorNumber, string message):base(message) { ErrorNumber = errorNumber; }
        public EvmRtException(int errorNumber, string message, Exception inner) : base(message, inner) { ErrorNumber = errorNumber; }

         public EvmRtException(string message) : base(message) { }
         public EvmRtException(string message, Exception inner) : base(message, inner) { }
    }
}
