﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBatisNet.DataMapper;

namespace Evm.Rtl
{
    /// <summary>
    /// 应用程序基类
    /// </summary>
    public abstract class AppBase
    {
       
        /// <summary>
        /// 数据映射
        /// </summary>
        public abstract SqlMapper MyMapper { get; }

        /// <summary>
        /// 应用名称
        /// </summary>
        public  abstract string Name { get; }

        /// <summary>
        /// 应用说明
        /// </summary>
        public abstract string Description { get; }

        /// <summary>
        /// 应用版本
        /// </summary>
        public abstract Version Version { get; }

        /// <summary>
        /// 应用作者
        /// </summary>
        public abstract string Author { get; }

    }
}
