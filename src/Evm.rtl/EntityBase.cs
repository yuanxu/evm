﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
namespace Evm.Rtl
{
    
    /// <summary>
    /// 实体基类
    /// </summary>
    public abstract class EntityBase
    {
        
        /// <summary>
        /// 实体是否通过了验证
        /// </summary>
        [JsonIgnore]
        public bool IsValid
        {
            get
            {
                var violations = this.RuleViolations;
                return violations == null || violations.Count() == 0;
            }
        }
        
        /// <summary>
        /// 获取验证失败内容
        /// </summary>
        [JsonIgnore]
        public IEnumerable<RuleViolation> RuleViolations
        {
            get
            {
                return from prop in TypeDescriptor.GetProperties(this).Cast<PropertyDescriptor>()
                       from attribute in prop.Attributes.OfType<ValidationAttribute>()
                       where !attribute.IsValid(prop.GetValue(this))
                       select new RuleViolation(prop.Name, attribute.FormatErrorMessage(string.Empty));
            }
        }
    }
}
