﻿using System;
using System.Collections.Generic;


namespace Evm.Rtl
{
    /// <summary>
    /// RTM引擎，同时也是一个DI容器
    /// </summary>
    public sealed partial class Engine
    {

        /// <summary>
        /// 解析类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Resolve<T>()
        {
            return container.Resolve<T>();
        }

        public object Resolve(string typeName)
        {
            return container.Resolve(ResolveType(typeName));
        }
        public object Resolve(Type t)
        {
            return container.Resolve(t);
        }

        private Dictionary<string, Type> m_typeCache = new Dictionary<string, Type>();
        public Type ResolveType(string typeName)
        {

            if (m_typeCache.ContainsKey(typeName))
                return m_typeCache[typeName];

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                var t = assembly.GetType(typeName);
                if (t != null)
                {
                    m_typeCache.Add(typeName, t);
                    return t;
                }
            }
            return null;
        }

        
    }
}
