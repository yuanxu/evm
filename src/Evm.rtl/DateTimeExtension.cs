﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Rtl
{
    /// <summary>
    /// 日期类型扩展类
    /// </summary>
    public static class DateTimeExtension
    {
        private static DateTime nullValue = Convert.ToDateTime("1900-1-1");

        /// <summary>
        /// 获取空值
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime NullValue(this DateTime dt)
        {
            return nullValue;
        }

        /// <summary>
        /// 是否是空值
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static bool IsNullValue(this DateTime dt)
        {
            return dt == nullValue;
        }

        /// <summary>
        /// 获取字符串值
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToNullableString(this DateTime dt)
        {
            if (IsNullValue(dt))
                return "";
            else
                return dt.ToString();
        }

        public static string ToNullableString(this DateTime dt, IFormatProvider provider)
        {
            if (IsNullValue(dt))
                return "";
            else
                return dt.ToString(provider);
        }

        public static string ToNullableString(this DateTime dt, string format)
        {
            return IsNullValue(dt) ? "" : dt.ToString(format);
        }
        public static string ToNullableString(this DateTime dt, string format, IFormatProvider provider)
        {
            return IsNullValue(dt) ? "" : dt.ToString(dt.ToString(format, provider));
        }
    }
}
