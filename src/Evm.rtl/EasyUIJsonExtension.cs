﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace Evm.Rtl
{
    public static class EasyUIJsonExtension
    {
        
        public static string ToJson<T>(this PagedList<T> list,string datetimeFormatter="yyyy-MM-dd hh:mm:ss")
        {
            EvmDateTimeConverter isoDateTimeConverter = new EvmDateTimeConverter();

            isoDateTimeConverter.DateTimeFormat = datetimeFormatter;
            
            var data = JsonConvert.SerializeObject(list.Data, Formatting.Indented, isoDateTimeConverter);
            return data;
        }
        public static string ToPagedJson<T>(this PagedList<T> list, string datetimeFormatter = "yyyy-MM-dd hh:mm:ss")
        {
            return string.Format("{{\"total\":{0},\"rows\":{1} }}", list.RecordCount, ToJson(list,datetimeFormatter));
        }
        public static string ToJson<T>(this IList<T> list, string datetimeFormatter = "yyyy-MM-dd hh:mm:ss")
        {
            EvmDateTimeConverter isoDateTimeConverter = new EvmDateTimeConverter();

            isoDateTimeConverter.DateTimeFormat = datetimeFormatter;


            var data = JsonConvert.SerializeObject(list, Formatting.Indented, isoDateTimeConverter);
            return  data;
        }
        public static string ToPagedJson<T>(this IList<T> list, string datetimeFormatter = "yyyy-MM-dd hh:mm:ss")
        {
            return string.Format("{{\"total\":{0},\"rows\":{1} }}", list.Count, ToJson(list,datetimeFormatter));
        }
    }

    public class EvmDateTimeConverter : IsoDateTimeConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is DateTime)
            {
                DateTime dt = (DateTime)value;
                if (dt.IsNullValue())
                {
                    writer.WriteValue("");
                }
                else
                {
                    base.WriteJson(writer, value, serializer);
                }
            }
            else
            {
                base.WriteJson(writer, value, serializer);
            }
        }
    }
}
