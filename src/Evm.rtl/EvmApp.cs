﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Evm.Rtl
{
    public class EvmApp:Schema.App
    {
        public EvmApp()
        {
            ExecutedInstall = false;
            LastVersionString = "0.0.0.0";
            SyncedSchema = false;
        }
        /// <summary>
        /// 是否执行过InstallMethod
        /// </summary>
        public bool ExecutedInstall { get; set; }

        /// <summary>
        /// 上一个版本
        /// </summary>
        public string LastVersionString { get; set; }

        /// <summary>
        /// 是否同步过Schema 
        /// </summary>
        public bool SyncedSchema { get;set; }

        
    }

    public static class AppExtension
    {
        public static EvmApp ToEvmApp(this Schema.App app)
        {
            var evmapp = new EvmApp();
            evmapp.Assemblies = app.Assemblies;
            evmapp.Author = app.Author;
            evmapp.Description = app.Description;
            evmapp.Entities = app.Entities;
            evmapp.EntityDefinitions = app.EntityDefinitions;
            evmapp.ExecutedInstall = false;
            evmapp.Imports = app.Imports;
            evmapp.LastVersionString = "0.0.0.0";
            evmapp.Name = app.Name;
            evmapp.Namespace = app.Namespace;
            evmapp.Options = app.Options;
            evmapp.ServiceDefinitions = app.ServiceDefinitions;
            evmapp.Services = app.Services;
            evmapp.SyncedSchema = false;
            //evmapp.Version = app.Version;
            evmapp.VersionString = app.VersionString;
            return evmapp;
        }
    }
}
