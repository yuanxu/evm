﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using IBatisNet.DataMapper;

namespace Evm.Rtl
{
    /// <summary>
    /// 服务基类
    /// </summary>
    public abstract class ServiceBase
    {
        private ILog m_log;
        /// <summary>
        /// 日志记录器
        /// </summary>
        protected ILog Log
        {
            get
            {
                if (m_log == null)
                    m_log = LogManager.GetLogger(this.GetType());
                
                return m_log;
            }
        }
        /// <summary>
        /// 获取当前服务的映射
        /// </summary>
        protected abstract SqlMapper MyMapper { get; }

        /// <summary>
        /// 确保处于事务中运行
        /// </summary>
        protected void AssertInTransaction()
        {
            if (!MyMapper.IsSessionStarted)
                throw new TransactionException("方法需要运行于事务环境中，但是事务并未启动！");
        }
        /// <summary>
        /// 确保不在事务中运行
        /// </summary>
        protected void AssertNotInTransaction()
        {
            if (MyMapper.IsSessionStarted)
                throw new TransactionException("方法需要能能在事务环境中，但上下文中已经启动了事务！");
        }
        /// <summary>
        /// 确保事务未启动
        /// </summary>
        protected void AssertTransactionNotStart()
        {
            if (MyMapper.IsSessionStarted)
                throw new TransactionException("方法需要新的事务环境中，但上下文中已经启动了事务！");
        }
        protected void BeginTransaction()
        {
            MyMapper.BeginTransaction();
            
        }
        protected void Commit()
        {
            MyMapper.CommitTransaction();
            
        }

        protected void Rollback()
        {
            MyMapper.RollBackTransaction();
            
        }
    }
}
