<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Sample.NorthWind.Entity.Categories>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	新建产品分类
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  
    <fieldset>
        <legend>新建产品分类</legend>
        <%:Html.Partial("CategoriesEditor",Model) %>
    </fieldset>
    
</asp:Content>
