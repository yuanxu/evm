<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PagedList<Sample.NorthWind.Entity.Categories>>" %>
<%: Html.Grid(Model.Data)
       .Empty("没有数据")
       .Columns(
			column =>
            {
				column.For(c => Html.ActionLink(c.CategoryName.ToString(), "Edit", new { id=c.CategoriesID })).Named("类别名称").SortColumnName("CategoryName");
column.For(c => c.Description).Named("描述");
column.For(c => c.Picture).Named("图片");

			}).Sort((GridSortOptions)ViewData["sort"])
%>
