<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Sample.NorthWind.Entity.Categories>" %>
  <script src="../../Scripts/MicrosoftAjax.js" type="text/javascript"></script>
  <script src="../../Scripts/MicrosoftMvcValidation.js" type="text/javascript"></script>
  <%Html.EnableClientValidation(); %>
    <% Html.BeginForm(); %>
    <%: Html.Hidden("UrlReferrer",ViewData["UrlReferrer"]) %>
    <%: Html.ValidationMessage("error") %>
    <%: Html.Partial("ShowError") %>
    <%:Html.ValidationSummary() %>
        <div class="editor-label">
  <%:Html.LabelFor(m=>m.CategoryName) %>
</div>
<div class="editor-field">
  <%:Html.TextBoxFor(m=>Model.CategoryName) %>
    <%:Html.ValidationMessageFor(m=>Model.CategoryName) %>
</div>
<div class="editor-label">
  <%:Html.LabelFor(m=>m.Description) %>
</div>
<div class="editor-field">
  <%:Html.TextAreaFor(m=>Model.Description,4,25,null) %>
    <%:Html.ValidationMessageFor(m=>Model.Description) %>
</div>
<div class="editor-label">
  <%:Html.LabelFor(m=>m.Picture) %>
</div>
<div class="editor-field">
  <%:Html.FileFor(m=>Model.Picture,"") %>
    <%:Html.ValidationMessageFor(m=>Model.Picture) %>
</div>

       <p>
        <button type="submit">�ύ</button>
         <button onclick="window.location.href='<%=ViewData["UrlReferrer"] %>'" type="button">ȡ��</button>
      </p>
      <% Html.EndForm(); %>