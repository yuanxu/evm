<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Sample.NorthWind.Entity.Categories>" %>

  <asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    产品分类
  </asp:Content>

  <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>产品分类</h2>
    <%:Html.Partial("CategoriesDetails",Model) %>
  </asp:Content>
