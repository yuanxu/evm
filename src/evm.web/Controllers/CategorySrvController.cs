using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sample.NorthWind.Entity;
using Sample.NorthWind.Service;
using Evm.Rtl;
using Evm.Rtl.Mvc;
using Evm.Rtl.Mvc.Extensions;

namespace Sample.NorthWind.Web.Controllers
{
    [HandleError]
    public class CategoriesController : Controller
    {
		private ICategorySrv objSrv;
		public CategoriesController()
		{
			objSrv = Engine.Instance.Resolve<ICategorySrv>();
		}

		public JsonResult GetProductList(int catID,int pageIndex,int pageSize)
{
    return Json(objSrv.GetProductList(catID,pageIndex,pageSize));
}

		//
        // GET: /Categories/Create

        public ActionResult Create()
        {
            ViewData["UrlReferrer"] = Request.UrlReferrer == null ?Url.Action("Index") : Request.UrlReferrer.ToString();
            return View();
        } 

        //
        // POST: /Categories/Create

        [HttpPost]
        public ActionResult Create(Categories model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    objSrv.Add(model);
                    var url = Request["UrlReferrer"];
                    return Redirect(url);
                }
                else
                {
                    return View(model);
                }
            }
            catch(Exception e)
            {
                 ViewData["Error"] = e;
                return View(model);
            }
        }
		//
        // GET: /Categories/Details/5

        public ActionResult Details(int id)
        {
            return View(objSrv.GetByID(id));
        }
		//
        // GET: /Categories/Details/5

        public ActionResult Details2(Guid gid)
        {
            return View(objSrv.GetByGuid(gid));
        }
        //
        // GET: /Categories/Edit/5
 
        public ActionResult Edit(int id)
        {
			ViewData["UrlReferrer"] = Request.UrlReferrer == null ?Url.Action("Index") : Request.UrlReferrer.ToString();
            return View(objSrv.GetByID(id));
        }

        //
        // POST: /Categories/Edit/5

        [HttpPost]
        public ActionResult Edit(int id,Categories model)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    objSrv.Save(model);
					var url = Request["UrlReferrer"];
                    return Redirect(url);
                }
                else
                {
					return View(model); 
				}
            }
            catch(Exception e)
            {
                ViewData["Error"] = e;
                
                return View(model);
            }
        }
		//
        // GET: /Categories/

        public ActionResult Index(QueryOptions options)
        {
            ViewData["sort"] = options.ToGridSortOption();
            return View(objSrv.GetList(options.PageIndex,options.PageSize,options.Column,options.Direction));
        }
		//
        // GET: /Categories/Delete/5

        public ActionResult Delete(int id)
        {
			objSrv.DeleteByID(id);
            return RedirectToAction("Index");
        }

    }
}
