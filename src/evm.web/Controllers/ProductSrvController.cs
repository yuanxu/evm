using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sample.NorthWind.Entity;
using Sample.NorthWind.Service;
using Evm.Rtl;
using Evm.Rtl.Mvc;
using Evm.Rtl.Mvc.Extensions;

namespace Sample.NorthWind.Web.Controllers
{
    [HandleError]
    public class ProductSrvController : Controller
    {
		private IProductSrv objSrv;
		public ProductSrvController()
		{
			objSrv = Engine.Instance.Resolve<IProductSrv>();
		}

				//
        // GET: /ProductSrv/
		
        public ActionResult Index(QueryOptions options)
        {
            ViewData["sort"] = options.ToGridSortOption();
            return View(objSrv.GetList(options.PageIndex,options.PageSize,options.Column,options.Direction));
        }
[EvmAuthorize(Roles="Administrator",Users="")]
public ActionResult Create(Products obj)

{

    objSrv.Create(obj);
return View();

}


    }
}
