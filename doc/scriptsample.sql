if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('e_TaskLog') and o.name = 'FK_E_TASKLO_REFERENCE_E_TASK')
alter table e_TaskLog
   drop constraint FK_E_TASKLO_REFERENCE_E_TASK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('e_TaskLog')
            and   name  = 'idx_taskLog'
            and   indid > 0
            and   indid < 255)
   drop index e_TaskLog.idx_taskLog
go

if exists (select 1
            from  sysobjects
           where  id = object_id('e_TaskLog')
            and   type = 'U')
   drop table e_TaskLog
go

if exists (select 1
            from  sysobjects
           where  id = object_id('e_task')
            and   type = 'U')
   drop table e_task
go

/*==============================================================*/
/* Table: e_TaskLog                                             */
/*==============================================================*/
create table e_TaskLog (
   LogId                int                  identity,
   TaskId               int                  not null,
   NextRunTime          datetime             null,
   Status               int                  null,
   RunTime              datetime             null,
   Error                varchar(1000)        null,
   constraint PK_E_TASKLOG primary key (LogId)
)
go

/*==============================================================*/
/* Index: idx_taskLog                                           */
/*==============================================================*/
create index idx_taskLog on e_TaskLog (
TaskId ASC
)
go

/*==============================================================*/
/* Table: e_task                                                */
/*==============================================================*/
create table e_task (
   TaskId               int                  identity,
   TaskName             nvarchar(50)         null,
   Description          nvarchar(200)        null,
   Processor            varchar(100)         null,
   Interval             int                  null,
   CycleType            int                  null default 0,
   AppendTime           datetime             null default getdate(),
   NextRunTime          datetime             null,
   PrevStatus           int                  null,
   Status               tinyint              not null default 1,
   constraint PK_E_TASK primary key (TaskId)
)
go

alter table e_TaskLog
   add constraint FK_E_TASKLO_REFERENCE_E_TASK foreign key (TaskId)
      references e_task (TaskId)
go
